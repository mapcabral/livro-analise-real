# Makefile para automatizar a compilacao de meus documentos TeX/LaTeX
DATE=`date +%Y-%b-%d-%a-%Hh%Mm``hostname`


# Colocar aqui o nome (base) dos arquivos 
TITLE = curso-analise-real
FILES = curso-analise-real.tex capa.tex sobreautores.tex prefacio.tex \
        cap1.tex cap2.tex cap3.tex cap4.tex  cap5.tex \
        cap6.tex cap7.tex cap8.tex cap9.tex cap10.tex\
        cap1-exercicios.tex cap2-exercicios.tex cap3-exercicios.tex \
	cap4-exercicios.tex cap5-exercicios.tex \
        cap6-exercicios.tex cap7-exercicios.tex cap8-exercicios.tex \
	cap9-exercicios.tex cap10-exercicios.tex\
	biblio.tex

default: editora-im

# Gerando livro com capa para editora do IM
editora-im: a4
	pdftk capa_Editora_IM_2021.pdf curso-analise-real-a4.pdf  cat output curso-analise-real-editora-IM.pdf
	ls -lh  curso-analise-real-editora-IM.pdf
#   pdftk mantém o índice.

all: index a4 guia
guia:
	latex guia-estudo-analise.tex
	dvips guia-estudo-analise -o
	pdflatex guia-estudo-analise.tex
index:
	latex  ${TITLE}.tex 
#	makeindex curso-analise-real.idx
#	latex  ${TITLE}.tex 

tgz: 
	touch ultimo-backup-$(DATE)
	cd ..; tar czvf tgzs/livro-analise-$(DATE).tgz livro-analise/ --exclude "*.pdf" --exclude "*.ps" --exclude "*.dvi" --exclude "*.aux" --exclude "*.log"  --exclude "*~"  --exclude "*.idx"  --exclude "*.ilg" 
	ls -lh ../tgzs/livro-analise*.tgz 

#${TITLE}.dvi dvi: ${TITLE}.tex ${FILES}

${TITLE}.dvi dvi: 
	latex  ${TITLE}.tex 

${TITLE}-a4.ps ps: index
	dvips -t a4 ${TITLE}.dvi -o ${TITLE}-a4.ps

${TITLE}-a4.pdf pdf: ${TITLE}-a4.ps
	ps2pdf -dALLOWPSTRANSPARENCY ${TITLE}-a4.ps ${TITLE}-a4.pdf

a4: ps pdf

draft: ${TITLE}.dvi
	dvips -t a4 ${TITLE}.dvi -o
	pstops -pa4 "4:0L@.707(21cm,0)+1L@.707(21cm,14.85cm),2R@.707(0,29.7cm)+3R@.707(0,14.85cm)" ${TITLE}.ps ${TITLE}_tmp.ps
	mv ${TITLE}_tmp.ps ${TITLE}.ps

book a5: 
	dvips -t a4 ${TITLE}.dvi -o
	psbook ${TITLE}.ps | pstops -pa4 "4:0L@.707(21cm,0)+1L@.707(21cm,14.85cm),2R@.707(0,29.7cm)+3R@.707(0,14.85cm)" > ${TITLE}_tmp.ps
	mv ${TITLE}_tmp.ps ${TITLE}-a5.ps
	ps2pdf -dALLOWPSTRANSPARENCY ${TITLE}-a5.ps ${TITLE}-a5.pdf

bbook: ${TITLE}.dvi
	dvips -t a4 ${TITLE}.dvi -o
	psbook -s32 ${TITLE}.ps | pstops -pa4 "4:0L@.707(21cm,0)+1L@.707(21cm,14.85cm),2R@.707(0,29.7cm)+3R@.707(0,14.85cm)" > ${TITLE}_tmp.ps
	mv ${TITLE}_tmp.ps ${TITLE}.ps

clean:
	rm -f ${TITLE}.aux ${TITLE}.bbl ${TITLE}.blg ${TITLE}.fot ${TITLE}.idx ${TITLE}.ilg ${TITLE}.ind ${TITLE}.log ${TITLE}.lof ${TITLE}.toc ${TITLE}.out

cleanall: clean
	rm -f ${TITLE}.dvi ${TITLE}.ps ${TITLE}.pdf
	rm -i ${TITLE}*.ps ${TITLE}*.pdf ${TITLE}*.djvu 

spell: spell1 spell2 spell3 spell4
spell1:
	aspell -l PT_BR  check cap1.tex
	aspell -l PT_BR  check cap2.tex
	aspell -l PT_BR  check cap3.tex
	aspell -l PT_BR  check cap4.tex
	aspell -l PT_BR  check cap5.tex
	aspell -l PT_BR  check cap6.tex
	aspell -l PT_BR  check cap7.tex
	aspell -l PT_BR  check cap8.tex
	aspell -l PT_BR  check cap9.tex
	aspell -l PT_BR  check cap10.tex
spell2:	
	aspell -l PT_BR  check cap1-exercicios.tex
	aspell -l PT_BR  check cap2-exercicios.tex
	aspell -l PT_BR  check cap3-exercicios.tex
	aspell -l PT_BR  check cap4-exercicios.tex
	aspell -l PT_BR  check cap5-exercicios.tex
	aspell -l PT_BR  check cap6-exercicios.tex
	aspell -l PT_BR  check cap7-exercicios.tex
	aspell -l PT_BR  check cap8-exercicios.tex
	aspell -l PT_BR  check cap9-exercicios.tex
	aspell -l PT_BR  check cap10-exercicios.tex
spell3:	
	aspell -l PT_BR  check capa.tex
	aspell -l PT_BR  check prefacio.tex
	aspell -l PT_BR  check sobreautores.tex
	aspell -l PT_BR  check biblio.tex 
spell4:	
	aspell -l PT_BR  check guia-estudo-analise.tex

