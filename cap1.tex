\chapter{Noções de Teoria dos Conjuntos}
% Retirei
%\setcounter{page}{1}
%\renewcommand{\thepage}{\arabic{page}}

\section{Conjuntos e operações.}

A noção intuitiva que se tem da palavra conjunto nos é satisfatória e uma
apresentação rigorosa da Teoria dos Conjuntos é difícil e além dos objetivos do
curso. Para detalhes leia o clássico [Ha].

\begin{definicao}
Um {\bf conjunto}%
\index{Conjunto}
é constituído de objetos chamados {\bf elementos}.%
\index{Elemento}
Usamos a notação $x\in A$ (lê-se $x$ pertence a $A$) para dizer que $x$ é um
elemento do conjunto $A$. Se $x$ não é um elemento de $A$, então escrevemos $x
\notin A$ (lê-se $x$ não pertence a $A$).
\end{definicao}

Uma forma de caracterizar um conjunto é através da lista dos seus elementos,
escrevendo-os separados por vírgulas ``,'' no interior de duas chaves ``$\{$''
e ``$\}$''.

\begin{exemplo}
\label{Ex: Um conjunto}
Seja $A$ o conjunto cujos elementos são os números $1,\ 2,\ 3,\ 4,\ 5$ e $6$.
Escrevemos $A=\{1,2,3,4,5,6\}$. Temos $1\in A,\ 2\in A$ e $7\notin A$.
\end{exemplo}

Outra maneira de caracterizar um conjunto é através de uma propriedade $P$
possuída por todos os seus elementos e apenas por estes 
(na Seção~\ref{Sec: facil}
faremos mais considerações sobre isto). 
Escrevemos neste caso $\{x\ ;\ P(x) \}$, $\{
x\ |\ P(x)\}$ ou $\{x\ :\ P(x)\}$ (lê-se o conjunto dos elementos $x$ tais que
$P(x)$ é verdadeira, ou ainda, dos elementos $x$ que possuem a propriedade $P$).
Salientamos que a letra $x$ é arbitrária de modo que $\{x\ ;\ P(x)\}=\{y \ ;\
P(y)\}$.

\begin{exemplo}
Seja $P$ a propriedade ``é um número presente na face de um dado'' e seja $A=
\big\{x\ ;\ P(x)\big\}$. Então $A=\{1,2,3,4,5,6\}$, {\em i.e.}%
\footnote{{\em i.e.}, abreviação de ``{\em id est}'' que, em latim, significa
``isto é''.},
$A$ é o mesmo conjunto do Exemplo \ref{Ex: Um conjunto}.
\end{exemplo}

\begin{definicao}
Dizemos que $A$ é um {\bf subconjunto}%
\index{Subconjunto}
de $B$ ou que $A$ é uma {\bf parte} de $B$, ou ainda, que $A$ {\bf está contido
em} $B$ e escrevemos $A\subset B$ se todo elemento de $A$ pertence a $B$.
Dizemos também que $B$ {\bf contém} $A$ e escrevemos $B\supset A$. 
\end{definicao}

\begin{definicao}
Quando
$A\subset B$ e $B\subset A$, os conjuntos $A$ e $B$ são ditos {\bf iguais} e
escrevemos $A=B$. Caso contrário eles são {\bf diferentes} e escrevemos $A\neq
B$. A notação $A\subsetneq B$ (ou $B\supsetneq A$) é uma abreviação para $A
\subset B$ com $A\neq B$, neste caso dizemos que $A$ é um {\bf subconjunto
próprio}%
\index{Subconjunto!próprio} de $B$.
\end{definicao}

\begin{observacao} 
 Para provar que dois conjuntos $A$ e $B$ são iguais deve-se provar
que $A\subset B$ e depois que $B\subset A$.
\end{observacao} 

\begin{exemplo}
Sejam $A=\{2,4,6\}$ e $B=\{1,2,3,4,5,6\}$. Temos que $A\subsetneq B$.
\end{exemplo}

\begin{exemplo}
Sejam $A$ o conjunto dos números inteiros múltiplos de $4$ e $B$ o conjunto dos
números pares. É óbvio que $A\subset B$ porém, vamos demonstrar esta afirmação.
O primeiro passo consiste em interpretar a definição do conjunto $A$. Um número
inteiro $n$ é múltiplo de $4$ se $n/4$ é inteiro, ou equivalentemente, se existe
um inteiro $m$ tal que $n=4m$. Logo,
\[
A=\{n\ ;\text{ existe um inteiro }m\text{ tal que }n=4m\}.
\]
Analogamente,
\[
B=\{n\ ;\text{ existe um inteiro }m\text{ tal que }n=2m\}.
\]
Estamos preparados para a demonstração. Seja $n\in A$. Então existe um inteiro
$m$ tal que $n=4m=2(2m)$. Como $m$ é inteiro, $2m$ também é. Concluímos que $n
\in B$.

Como $n$ é um elemento arbitrário de $A$ (além de $n\in A$ não fizemos nenhuma
hipótese sobre $n$) concluímos que qualquer que seja $n\in A$ temos $n\in B$,
{\em i.e}, que todo elemento de $A$ pertence a $B$, ou seja, que $A\subset B$.
Isto termina a demonstração.
\end{exemplo}

\begin{exemplo}
Sejam $A=\{0,1,2\}$ e $B=\{1,2,3,4\}$. Pergunta: $A\subset B$? Por quê?
Resposta: Não, pois $0\in A$ e $0\notin B$.
\end{exemplo}

De maneira geral, se $A$ não é um subconjunto de $B$ significa que existe pelo
menos um elemento de $A$ que não pertence a $B$.

\begin{definicao}
O \textbf{conjunto vazio},
\index{Conjunto!vazio}%
denotado por $\varnothing$, 
é um conjunto que não possui nenhum elemento, ou seja, não existe $x$
tal que $x\in\varnothing$. 
%Existe um conjunto especial chamado de {\bf vazio},%
%\index{Conjunto!vazio}
%denotado por $\varnothing$, 
%que não possui nenhum elemento, ou seja, não existe $x$
%tal que $x\in\varnothing$. 
\end{definicao} 

Uma propriedade interessante do conjunto vazio é que
ele é subconjunto de qualquer conjunto. Vejamos isto mais precisamente.
Suponhamos que exista um conjunto $A$ tal que $\varnothing$ não seja subconjunto
de $A$. Pelo que vimos anteriormente, isto significa que existe algum elemento
$x\in\varnothing$ tal que $x\notin A$. Mas, por definição de vazio, não podemos
ter $x\in\varnothing$. Esta contradição nos obriga a concluir que $\varnothing
\subset A$ pois, senão, chegaríamos a uma conclusão absurda.

Acabamos de mostrar que $\varnothing\subset A$ usando um argumento do tipo
``\textbf{demonstração por absurdo}'' ou 
``\textbf{demonstração por contradição}''.
\indexabsurdo% 
Neste tipo de argumento supomos inicialmente que a conclusão desejada seja falsa
e, a partir desta hipótese, chegamos a um absurdo. Desta forma, somos obrigados
a admitir que a suposição é falsa e, portanto, que a conclusão desejada é
verdadeira.

Existem conjuntos cujos elementos são conjuntos como mostra o próximo exemplo.

\begin{exemplo}
\label{Ex: Conjuntos de conjuntos}
Sejam $A=\{1,2\}$, $B=\{3\}$ e $\C=\{A,B\}$. Tente se convencer de que todas as
afirmativas abaixo são verdadeiras.
\[
A\in\C,\quad B\in\C,\quad\{A\}\subset\C,\quad\{B\}\subset\C,\quad 1\notin\C,
\quad 2\notin\C,\quad 3\notin\C.
\]
Perceba ainda que é errado dizer $\{2\}\subset\C$, $\{3\}\subset\C$ ou $\big\{
\{2\}\big\}\subset\C$. Entretanto, é verdade que $\big\{\{3\}\big\}\subset\C$
(esta é simplesmente a quarta das afirmações acima).
\end{exemplo}

%\begin{exemplo}
%\label{Ex: Conjuntos de conjuntos}
%Sejam $A=\{1,2\}$, $B=\{1,A\}$ e $C=\{1,2,A\}$. Tente se convencer de que todas
%as afirmativas abaixo são verdadeiras:
%\begin{enumerate}
%\item $1\in A,\quad \{1\}\subset A,\quad \{1\}\subsetneq A,\quad
%\{1,2\}\subset A,\quad A\subset\{1,2\}$;
%\item $1\in B,\quad\{1\}\subset B,\quad A\in B,\quad\{1,2\}\in B,\quad
%\{A\}\subset B,\quad\{\{1,2\}\}\subset B$;
%\item $2\in C,\quad\{2\}\subset C,\quad A\in C,\quad\{A\}\subset C,\quad
%A\subset C$.
%\end{enumerate}
%
%Agora convença-se de que todas as afirmações abaixo são falsas.
%\begin{enumerate}
%\item $1\subset A,\quad\{1\}\in A$;
%\item $1\subset B,\quad\{1\}\in B,\quad A\subset B,\quad\{1,2\}\subset B,\quad
%\{A\}\in B,\quad\{\{1,2\}\}\in B$;
%\item $2\in B,\quad\{2\}\subset B$;
%\item $2\subset C,\quad\{2\}\in C,\quad\{A\}\in C$.
%\end{enumerate}
%\end{exemplo}

\begin{definicao} 
\label{Def: colecao}
Quando $\C$ é um conjunto de conjuntos (para simplificar a linguagem)
%, muitas vezes 
dizemos que $\C$ é uma {\bf coleção},%
\index{Coleção}
uma {\bf classe}%
\index{Classe}
ou uma {\bf família}%
\index{Família}
de conjuntos. 
Elementos de $\C$ são comumente chamados de {\bf membros}.%
\index{Membro}
\end{definicao} 

Para famílias utiliza-se também notação especial 
(como veremos na Seção~\refpag{Sec: Familia}). 
Por falar em conjuntos de conjuntos...

\begin{definicao}
\label{Def: Conjunto das partes}
Seja $A$ um conjunto. A coleção de todos os subconjuntos de $A$ é dita {\bf
conjunto das partes}%
\index{Conjunto!das partes}
de $A$ e é denotada por $\P(A)$ ou por $2^A$. Em símbolos,
\[
\P(A)=\{B\ ; B\subset A\}.
\]
Portanto, $B\in\P(A)$ se, e somente se, $B\subset A$.
\end{definicao}

\begin{exemplo}
Temos que $\P(\varnothing)=\{\varnothing\}$. 
Note que $\varnothing\neq\P(\varnothing)$ (porque?).
Se $A=\{1\}$, então $\P(A)=\big\{\varnothing,\{1\}\big\}$.
\end{exemplo}

%\section{Operações com conjuntos.}

\begin{definicao}
Sejam $A$ e $B$ dois conjuntos. Existe um conjunto, chamado {\bf união}%
\index{Conjunto!união}%
\index{União}
\index{Reunião}
ou {\bf reunião} de $A$ e $B$ (denotado por $A\cup B$), cujos elementos
pertencem a $A$ \underline{ou} a $B$. Também existe um conjunto chamado {\bf
interseção}%
\index{Conjunto!interseção}%
\index{Interseção}
de $A$ e $B$ (denotado por $A\cap B$) cujos elementos pertencem a $A$
\underline{e} a $B$. Em outros termos
\[
A\cup B=\{x\ ;\ x\in A\text{ ou }x\in B\}\quad\text{e}\quad A\cap B=\{x\ ;\ x\in
A\text{ e }x\in B\}.
\]
\end{definicao}

De maneira geral, fazemos a seguinte definição.

\begin{definicao}
\label{Def: Uniao e intersecao}
Se ${\cal C}$ é uma coleção não vazia de conjuntos, então a {\bf união}%
\index{Conjunto!união}%
\index{União}
ou {\bf reunião}%
\index{Reunião}
da coleção $\C$ é formado pelos elementos que pertencem a \underline{pelo menos
um} membro de $\C$. Em símbolos,
\[
\bigcup_{A\in\C}A=\{x\ ;\ \text{existe }A\in\C\text{ tal que }x\in A\}.
\]
A {\bf interseção}%
\index{Conjunto!interseção}%
\index{Interseção}
da coleção $\C$ é constituída pelos elementos que pertencem a \underline{todos}
os membros de $\C$. Em símbolos,
\[
\bigcap_{A\in\C}A=\{x\ ;\ x\in A\text{ para todo }A\in\C\}.
\]
\end{definicao}

Por definição $A\cap B\cap C=\{x\ ;\ x\in A\text{ e }x\in B\text{ e }x\in C\}$.
Neste caso podemos substituir o conectivo ``e'' por uma vírgula ``,'' escrevendo
$A\cap B\cap C=\{x\ ;\ x\in A,\ x\in B\text{ e }x\in C\}$. Porém, o conectivo
``ou'' é sempre preservado.

\begin{exemplo}
Sejam $A=\{1,2,3\}$ e $B=\{1,2,4,8\}$. Temos
\(A\cup B=\{1,2,3,4,8\}\)
e
\(A\cap B=\{1,2\}.\)
\end{exemplo}

\begin{definicao}
Sejam $A$ e $B$ conjuntos. O conjunto {\bf diferença}%
\index{Diferença!de dois conjuntos}%
\index{Conjunto!diferença}
entre $A$ e $B$ (denotado por $A\setminus B$ ou $A-B$) é constituído pelos
elementos de $A$ que não pertencem a $B$. Em símbolos,
\(
A\setminus B=\{x\ ;\ x\in A\text{ e }x\notin B\}.
\)
\end{definicao}

\begin{definicao}
Quando trabalhamos apenas com subconjuntos de um determinado conjunto $X$
(subentendido no contexto) definimos o {\bf complementar}%
\index{Complementar}%
\index{Conjunto!complementar}
de $A$ por $X\setminus A$ e o denotamos $A^\complement$.
\end{definicao}

Dissemos anteriormente que um conjunto pode ser definido pela lista de seus
elementos. Devemos ressaltar que a ordem dos elementos na lista não importa e
que repetições são irrelevantes. Desta forma,
\(
\{a,b,c\}=\{b,a,c\}=\{c,a,b\}=\{a,a,b,c\}.
\)
Quando queremos que a ordem ou repetições sejam relevantes usamos o conceito de
par ordenado. 

\begin{definicao} 
Dados dois objetos $a$ e $b$ definimos o {\bf par ordenado}%
\index{Par ordenado}
$(a,b)$ cuja primeira {\bf coordenada}%
\index{Coordenada}
é $a$ e a segunda é $b$. Dois pares ordenados $(a,b)$ e $(c,d)$ são iguais se
eles forem iguais coordenada por coordenada, {\em i.e.},
\[
(a,b)=(c,d)\quad\text{se, e somente se,}\quad a=c\text{ e }b=d.
\]
\end{definicao} 

Repare que $(a,b)\neq(b,a)$ salvo se $a=b$ e que $(a,a)\neq a $. De maneira
análoga definimos {\bf triplas ordenadas}%
\index{Tripla ordenada}
$(a,b,c)$ ou \textbf{$\mathbf{n}$-uplas ordenadas}%
\index{n-uplas ordenadas}
$(a_1,\dots,a_n)$.

\begin{definicao} Dados dois conjuntos $A$ e $B$ existe um conjunto chamado de
{\bf produto cartesiano}%
\index{Produto!cartesiano}
de $A$ e $B$ (denotado $A\times B$) formado pelos pares ordenados $(a,b)$ tais
que $a\in A$ e $b\in B$. Em símbolos:
\(
A\times B=\{(a,b)\ ;\ a\in A\text{ e } b\in B\}.
\)
\end{definicao}
Em particular, podemos definir $A\times A$ e, por simplicidade, o denotamos
$A^2$. De maneira análoga definimos $A\times B\times C=\{(a,b,c)\ ;\ a\in A,\
b\in B\text{ e }c\in C\}$, $A^3=A\times A\times A$, $A^n=A\times\cdots\times A$
($n$ vezes).

%\section{Simplificando a escrita.}
\footnotetext[1]{Este neologismo é derivado de outro em inglês {\em iff} que significa
{\em if and only if}. Foi o matemático Halmos%
\index{Halmos}
que o inventou. A ele devemos
também o pequeno quadrado que indica final de demonstração. \\
Paul Richard Halmos: $\star$ 03/03/1916, Budapeste, Hungria.}%
\begin{observacao} 
Repetidas vezes usamos expressões do tipo ``existe'', ``para todo'', ``qualquer
que seja'', etc. Para simplificar a escrita destas expressões introduziremos
alguns símbolos que as representam, a saber:

\begin{savenotes}
\begin{tabular}{ll}
$\exists$ & significa ``existe'';\\
$\exists!$ & significa ``existe um único'';\\
$\forall$ & significa ``para todo'' ou ``qualquer que seja'';\\
$\imp$ & significa ``se ... então ...'' ou ``implica que'';\\
$\sse$ ou ``sse''\index{Sse}\footnotemark
& significa ``se, e somente se,''.%
\end{tabular}
\end{savenotes}
\end{observacao} 
%Temos também
%\[
%\bigcap_{A\in{\cal C}}A=\{x\ ;\ x\in A\ \ \forall A\in{\cal C}\}\quad\text{e}
%\quad
%\bigcup_{A\in{\cal C}}A=\{x\ ;\ \exists A\in{\cal C}\text{ tal que }x\in A\}.
%\]
Desta maneira, podemos escrever que, por definição, $A\subset B$ sse
\(
x\in A\quad\imp\quad x\in B.
\)

\section{$\star$ Teoria dos conjuntos é fácil?}
\label{Sec: facil}

Não entramos nos fundamentos lógicos da Teoria dos Conjuntos e tudo parece
trivial e familiar. Mas (in)felizmente a Teoria dos Conjuntos não é tão fácil
como possa parecer. Por exemplo, nossa exposição apresenta uma inconsistência
lógica, ou paradoxo, conhecido como Paradoxo de Russel%
\index{Paradoxo de Russel}%
\footnote{Bertrand Arthur William Russell, $\star$ 18/05/1872, Ravenscroft, País
de Gales - $\dagger$ 02/02/1970, Penrhyndeudraeth, País de Gales}.

Logo na primeira seção dissemos que dada uma propriedade $P$ podemos definir,
ou melhor, existe o conjunto $A$ dos elementos que possuem a propriedade $P$ e
escrevemos
\[
A=\big\{x\ ;\ P(x)\big\}.
\]
Ora, não há nada mais razoável.

Nada nos impede de considerar conjuntos cujos elementos são conjuntos (como já
fizemos ao introduzir coleções) e de questionar se um conjunto é elemento dele
mesmo. Como exemplo, considere o conjunto $C$ de todos objetos que não são
bolas. Ora, $C$ não é uma bola, logo, $C\in C$. Vejamos como isto gera um
paradoxo.

Diremos que um conjunto $X$ é {\bf normal} se ele não pertence a si próprio,
{\em i.e.}, se $X\notin X$. Seja $N$ o conjunto dos conjuntos normais:
\[
N=\{X\ ;\ X\text{ é normal}\}=\{X\ ;\ X\notin X\}.
\]

Perguntamo-nos se $N$ é normal. Existem duas respostas possíveis: sim ou não.
Vamos analisar cada uma delas.

\fbox{$1^{\underline a}$ possibilidade: $N$ \textbf{é}
normal.}
Por definição, $N$ é o conjunto dos conjuntos normais e, sendo ele próprio
normal, temos que $N\in N$. Isto implica, por definição de conjunto normal, que
$N$ \underline{não é} normal.
Temos então uma contradição! Pode-se pensar que este argumento seja apenas uma
demonstração por absurdo que mostra que a primeira possibilidade não funciona e
então devemos concluir que é a segunda que é a boa. Vejamos.

\fbox{$2^{\underline a}$ possibilidade: $N$ \textbf{não é}
normal.}
Pela definição de $N$, e como $N$ não é normal, devemos ter $N\notin N$. Logo,
por definição de conjunto normal, concluímos que $N$ \underline{é} normal.
Novamente temos uma contradição. Nenhuma das duas possibilidades é possível -
paradoxo!

Para eliminar este paradoxo da Teoria dos Conjuntos (que é o pilar de toda a
Matemática) uma solução é a seguinte. Ao invés de admitir que dada uma
propriedade $P$ existe o conjunto dos elementos que possuem a propriedade $P$,
admitimos que dada uma propriedade $P$ e um conjunto $A$ existe o subconjunto
dos elementos de $A$ que possuem a propriedade $P$. Escrevemos $\big\{x \in A\ ;
\ P(x)\big\}$. Feito isto o argumento usado no Paradoxo de Russel se transforma
em um teorema (veja \excpag{Exc: Universo}) segundo o qual não existe o
conjunto de todas as coisas ou, de forma mais ``poético-filosófica'', ``nada
contém tudo''. Boa  viagem!\index{Viagem}

\section{Funções.}

Todos sabemos que o valor da prestação de uma televisão comprada em 12 parcelas
iguais e sem juros depende do seu preço à vista. Por isto, dizemos que o valor
da prestação é função do preço à vista. Neste caso, se $x$ é o preço à vista,
então o valor da prestação é $x/12$. A função ``valor da prestação'' a cada
``valor à vista'' $x$ associa o ``valor da prestação'', dado por $x/12$. De
maneira geral, uma função associa, através de uma regra precisa, cada elemento
de um conjunto a um único elemento de outro conjunto (os dois conjuntos em
questão podem ser iguais).

O exemplo anterior é de uma função numérica definida através de uma fórmula, mas
nem toda função é deste tipo. Por exemplo, cada pessoa possui um único tipo
sanguíneo, logo, podemos considerar a função que a cada elemento do conjunto das
pessoas associa o seu tipo sanguíneo que é um elemento do conjunto $\{A,B,AB,
O\}$. Mudando a regra a função muda. Assim, a função anterior é diferente da
função que a cada pessoa associa o tipo sanguíneo do pai.

\begin{definicao}
\label{Def: Funcao}
Sejam $A$ e $B$ dois conjuntos não vazios. Uma {\bf função}%
\index{Função}
$f:A\rightarrow B$ (lê-se função $f$ de $A$ em $B$) é definida por uma regra de
associação, ou relação, entre elementos de $A$ e $B$ que a cada $x\in A$ associa
um único elemento $f(x)$ (lê-se $f$ de $x$) em $B$, dito {\bf imagem}%
\index{Imagem}
\index{Função!imagem}
de $x$ por $f$. O conjunto $A$ é o {\bf domínio}%
\index{Domínio}
\index{Função!domínio}
de $f$ enquanto que $B$ é o {\bf contradomínio}%
\index{Contradomínio}
\index{Função!contradomínio}
de $f$.
\end{definicao}

Note que não pode haver exceção à regra: todo $x\in A$ possui uma imagem $f(x)
\in B$. Por outro lado, pode existir $y\in B$ que não seja imagem de nenhum $x
\in A$. Note também que, dado $x\in A$, não pode haver ambiguidade com respeito
a $f(x)$. Entretanto, o mesmo elemento $y\in B$ pode ser imagem de mais de um
elemento de $A$, {\em i.e.}, pode ocorrer $f(x_1)=f(x_2)$ com $x_1\neq x_2$.

\begin{exemplo}
\label{Ex: Funcao}
Sejam $A=\{\text{\sf alunos da UFRJ}\}$, $B=\{\text{\sf números inteiros}\}$.
Como exemplo de função, temos $f:A\rightarrow B$ que a cada $x\in A$ associa seu
ano de nascimento. Outro exemplo é a função $g:A\rightarrow B$ que a cada $x\in
A$ associa seu ano de entrada na UFRJ.
\end{exemplo}

\begin{exemplo}
Seja $A=\{\text{\sf pessoas}\}$. Se a cada $x\in A$ fazemos corresponder $f(x)
\in A$ de maneira que $f(x)$ seja irmão de $x$, então $f$ não é uma função por
duas razões. Primeiro por exceção pois nem toda pessoa tem irmão. Segundo por
ambiguidade pois existem pessoas que têm mais de um irmão.
\end{exemplo}

\begin{definicao} 
Sejam $f,g:A\rightarrow B$ duas funções.
Dizemos que $f$ e $g$ são \textbf{iguais} se são dadas pela mesma regra de
associação, ou seja, se
\[
f(x)=g(x)\qquad\forall x\in A.
\]
\end{definicao} 
A condição acima só tem sentido (podendo ser falsa) se $f$ e $g$ tiverem o mesmo
domínio (no caso $A$). No entanto, é dispensável que $f$ e $g$ tenham o mesmo
contradomínio. Por esta razão, podemos considerar iguais duas funções de
contradomínios diferentes. Desta forma, a função
\[
h:\{\text{alunos da UFRJ}\}\rightarrow\{\text{números inteiros positivos}\},
\]
que a cada $x\in\{\text{alunos da UFRJ}\}$ associa seu ano de entrada na UFRJ é
igual a função $g$ do Exemplo $\ref{Ex: Funcao}$.
Mais delicado é considerar que funções de domínios diferentes sejam iguais.
Entretanto, cometemos este abuso quando, por exemplo, o domínio de uma função
contém o domínio da outra. Quando a prudência mandar, devemos lidar com os
conceitos de restrição e extensão.


\begin{definicao}
Sejam $f:A\rightarrow B$ e $g:C\rightarrow B$. Dizemos que $f$ é uma {\bf
restrição}
\index{Restrição}%
\index{Função!restrição}%
de $g$ ou que $g$ é uma {\bf extensão}
\index{Função!extensão}%
\index{Extensão}%
de $f$ se $A\subset C$ e $f(x)=g(x)$ para todo $x\in A$. Neste caso escrevemos
$f=\rest{g}{A}$. 
\end{definicao}

%No mesmo espírito das considerações que precedem a definição anterior é
%dispensável que $B\subset D$ ou $D\subset B$.
\begin{definicao} 
   \index{Conjunto!de funções}
   Dados dois conjuntos $A$ e $B$, denotamos por $\F(A;B)$ o
conjunto de todas as funções $f:A\rightarrow B$. 
\end{definicao} 

\begin{definicao} 
\label{Def: funcar}
Dado $A\subset C$, definimos a 
\textbf{função característica}%
\index{Função!característica}
ou 
\textbf{indicadora} 
\index{Função!indicadora}%
de $A$ por
$I_A: C\rightarrow\{0,1\}$ 
(também denotada por $\chi_A$)
 por 
% $I_A(x)=1$, se $x\in A$, e $I_A(x)=
%0$, se $x\notin A$. 
$I_A(x)=
\begin{cases} 
    0; & \text{ se } x\not\in A;\\
    1; & \text{ se } x\in A.\\

\end{cases}
$
\end{definicao} 
A função indicadora (ou característica)
é muito utilizada em teoria da integração e em probabilidade.
Podemos escrever que $I:\P(C)\rightarrow\F(C;\{0,1\})$ 
ou 
$I\in\F(\P(C);\F(C;\{0,1\}))$, 
pois $I$
associa a cada subconjunto $A\in\P(C)$ a função $I_A$.

\begin{definicao}
\label{Def: Imagem direta}
Seja $f:A\rightarrow B$. % e $C\subset A$. 
Definimos
$\widetilde f:\P(A)\rightarrow \P(B)$
para cada $C\in\P(A)$ (ou, o que é a mesma coisa, para cada $C\subset A$) 
por 
\[
\widetilde f(C)=\big\{y\in B\ ;\ \exists x\in C\text{ tal que }f(x)=y\}=\{f(x)\ ;\ x\in C
\big \},
\]
a  \textbf{imagem}
ou  \textbf{imagem direta}
\index{Imagem}%
\index{Imagem!direta}%
\index{Conjunto!imagem}%
de $C$ por $f$.
Abusamos a notação e escrevemos simplesmente $f(C)$ (sem o til). 
Em particular, o conjunto $f(A)$ é chamado de {\bf imagem}
de $f$.
\end{definicao}

\begin{definicao}
\label{Def: Imagem inversa}
Seja $f:A\rightarrow B$. 
Definimos
$\widetilde{f^{-1}}:\P(B)\rightarrow \P(A)$
para cada $C\in\P(B)$ (ou, o que é a mesma coisa, para cada $C\subset B$) 
por 
\[
\widetilde{f^{-1}}(C)=\big\{x\in A\ ;\ f(x)\in C\big\},
\]
a {\bf imagem inversa}%
\index{Imagem!inversa}
ou {\bf pré-imagem}%
\index{Pré-imagem}
de $C$ por $f$.
Abusamos a notação e escrevemos simplesmente $f^{-1}(C)$ (sem o til). 
Outros abusos são:
$f^{-1}(y)$ (em vez de $\widetilde{f^{-1}}(\{y\})$) e
$x=f^{-1}(C)$ (em vez de $\widetilde{f^{-1}} (C)=\{x\}$).
\end{definicao}

\begin{exemplo} 
Considere $f:\R\rightarrow\R$ definido por $f(x)=|x|$.
Então $f([-2,2])=[0,2]$, $f([-5,1))=[0,5]$. Além disso, $f^{-1}((1,2)) = 
(1,2)\cup(-2,-1)$, $f^{-1}(3)=\{3, -3\}$, 
$f^{-1}\left((-3,-1)\right)=\varnothing$, 
$f^{-1}(0)=0$.
\end{exemplo} 

\begin{definicao}
Uma função $f:A\rightarrow B$ é dita {\bf sobrejetiva}%
\index{Função!sobrejetiva}
se $f(A)=B$, ou seja, se qualquer que seja $y\in B$, existe $x\in A$ tal que
$f(x)=y$.
\end{definicao}
Ao se verificar a sobrejetividade de uma
função, deve estar claro qual conjunto está sendo considerado como
contradomínio.
Modificando-o, uma função que não é sobrejetiva pode passar a ser.

\begin{exemplo}
Seja $A=\{a,b\}$. A função $f$, definida por $f(x)=x$ para todo $x\in A$, não é
sobrejetiva de $A$ em $\{a,b,c\}$ mas é sobrejetiva de $A$ em $\{a,b\}$. De
modo geral, toda função é sobrejetiva na sua imagem.
\end{exemplo}

\begin{definicao}
Uma função $f:A\rightarrow B$ é dita {\bf injetiva}%
\index{Função!injetiva}
ou {\bf injeção}%
\index{Injeção}
se para quaisquer $x,y\in A$ tais que $x\neq y$ temos $f(x)\neq f(y)$, ou
equivalentemente, se $x,y\in A$ são tais que $f(x)=f(y)$, então $x=y$; ou ainda,
se para todo $y\in f(A)$ existe um único $x\in A$ tal que $f(x)=y$.
\end{definicao}

\begin{definicao} 
%Faremos a seguinte convenção de terminologia. 
Dizemos que a função $f$ tem a
propriedade $P$ em $A$ se $\rest{f}{A}$ tem a propriedade $P$. 
\end{definicao} 
Por exemplo, dizer
que $f$ é injetiva em $A$ significa que $\rest{f}{A}$ é injetiva. Isto é muito usual,
sobretudo em conversas informais entre matemáticos. Entretanto, isto deve ser
usado com cuidado para não cairmos em armadilhas 
(veja \excpag{Exc: Pegadinha da continuidade}).
\index{Armadilha}
%(veja Exercício \ref{Exc:
%Pegadinha da continuidade} do Capítulo \ref{Cap: Limite e continuidade}).

\begin{definicao}
Uma função $f:A\rightarrow B$ é dita {\bf bijetiva}%
\index{Função!bijetiva}
ou {\bf bijeção}%
\index{Bijeção}
se ela é injetiva e sobrejetiva.
\end{definicao}

\begin{exemplo}
Sejam $A=\{1,2,3\}$, $B=\{2,4,6\}$ e $C=\{1,4,9,16\}$. Consideremos as funções
$f:A\rightarrow B$, $g:A \rightarrow C$ e $h:A\rightarrow A$ definidas por
\[
f(x)=2x,\quad g(x)=x^2,\quad h(x)=2\quad\forall x\in A.
\]
Temos que $f$ é injetiva e sobrejetiva e, portanto, bijetiva. Temos ainda que
$g$ é injetiva mas não é sobrejetiva e $h$ não é injetiva e nem sobrejetiva.
\end{exemplo}

\begin{definicao}
Sejam $f:A\rightarrow B$ e $g:C\rightarrow D$ tais que $f(A)\subset C$.
Definimos a {\bf função composta}%
\index{Função!composta}
$g\circ f:A\rightarrow D$ que a cada $x\in A$ associa $g\big(f(x)\big)\in D$.
\end{definicao}

A definição anterior faz sentido pois dado $x\in A$ temos que $f(x)\in f(A)$ e
como $f(A)\subset C$ temos $f(x)\in C$. Neste caso podemos aplicar $g$ e
encontrar $g(f(x))\in D$.

Observamos ainda que a operação de composição de funções é associativa, {\em
i.e.}, se $f:A\rightarrow B$, $g:C\rightarrow D$ e $h:E\rightarrow F$ com $f(A)
\subset C$ e $g(C)\subset E$, então temos
\[
\big((h\circ g)\circ f\big)(x)=(h\circ(g\circ f))(x)=h(g(f(x)))\quad\forall x\in
A.
\]

\begin{definicao} 
Para $f:A\rightarrow A$ definimos $f^n:A\rightarrow A$ por $f^n=f\circ\dots\circ
f$ ($n$ vezes).
\end{definicao} 

\begin{definicao}
\label{Def: Funcao inversa}
Sejam $f:A\rightarrow B$ e $g:B\rightarrow A$ tais que $(g\circ f)(x)=x$ para
todo $x\in A$ e $(f\circ g)(y)=y$ para todo $y\in B$. Dizemos que $f$ é {\bf
invertível},%
\index{Função!invertível}
que $g$ é a {\bf inversa}%
\index{Função!inversa}
de $f$ e escrevemos $g=f^{-1}$.
\end{definicao}

Não devemos confundir $f^{-1}$ da definição acima com 
$\widetilde{f^{-1}}$ da Definição \ref{Def: Imagem inversa}. Sempre
que aplicamos $f^{-1}$  
em conjuntos está subentendido que trata-se da
imagem inversa.
Quando se aplica $f^{-1}$ num elemento $y$, pode-se entender como
$f^{-1}(y)$, caso a inversa exista, ou $\widetilde{f^{-1}}(\{y\})$, 
a imagem inversa de um conjunto unitário.

Repare que intercambiando $f$ com $g$, $A$ com $B$ e $x$ com $y$ as hipóteses da
Definição \ref{Def: Funcao inversa} não mudam, porém a conclusão dirá que $f$ é
a inversa de $g$. Concluímos que $f$ é a inversa de $g$ se, e somente se, $g$ é
a inversa de $f$.

Se $f:A\rightarrow B$ é injetiva, então mesmo quando ela não for sobrejetiva,
ainda poderemos considerar sua função inversa $f^{-1}$ ficando subentendido que
o domínio de $f^{-1}$ é $f(A)$ (e não $B$). Desta forma $(f^{-1}\circ f)(x)=x$
para todo $x\in A$ e $(f\circ f^{-1})(y)=y$ para todo $y\in f(A)$.

\section{Famílias}
\label{Sec: Familia}

Dissemos anteriormente
(Definição~\refpag{Def: colecao})
que a palavra família pode ser usada para designar conjuntos de
conjuntos. De fato, este é o principal uso da palavra família mas não o único.
Na verdade, uma família é uma função para a qual usamos uma notação especial.

\begin{definicao}
\label{Def: Familia}
Sejam $I$ e $C$ conjuntos não vazios. Uma {\bf família}%
\index{Família}
$(A_i)_{i\in I}$ de elementos de $C$ é uma função $A:I\rightarrow C$ para a qual
denotamos por $A_i$ (em vez de $A(i)$) a imagem de $i$ por $A$. Dizemos que a
família está indexada pelo {\bf índice}%
\index{Indice@Índice}
$i\in I$, que $I$ é o {\bf conjunto de índices}%
\index{Conjunto!de índices}
e que $A_i$ é o $i$-ésimo elemento%
\index{Elemento!de uma família}
(ou membro)%
\index{Membro!de uma família}
da família. Quando $I$ é o conjunto dos números naturais substituímos a palavra
família por {\bf sequência}.%
\index{Sequência}
\end{definicao}

Os gramáticos que nos perdoem \index{Perdoar}  \sorriso\ 
mas usamos o sufixo ``ésimo'' em $i$-ésimo mesmo
quando $i$ não é um número cardinal.

Observe que na notação $(A_i)_{i\in I}$ não aparece o contradomínio $C$ da
função. Por isto, ao introduzirmos uma família, é obrigatório dizer que tipo de
objetos constituem o seu contradomínio. Por exemplo, uma família de pessoas é
uma função cujo contradomínio é um conjunto de pessoas. Da mesma forma, uma
família de macacos é uma função cujo contradomínio é um conjunto de macacos
(agora são os biólogos que hão de nos perdoar).

Como dito anteriormente, o uso mais frequente do termo família é quando o
contradomínio é uma coleção de conjuntos. Trata-se, então, de uma família de
conjuntos. Neste caso, existem notações especiais para a união e a interseção da
coleção. Se $(A_i)_{i\in I}$ é uma família de conjuntos, então a união e a
interseção da família são definidas, respectivamente, por
%\[
%\bigcup_{i\in I}A_i=\bigcup_{B\in\C}B\qquad\text{e}\qquad
%\bigcap_{i\in I}A_i=\bigcap_{B\in\C}B,
%\]
%sendo $\C$ a imagem de $A$. Desta forma, $x$ pertence a união da família $(A_i)
%_{i\in I}$ se, e somente se, existe $B\in\C$ tal que $x\in B$. Mas como $\C$ é a
%imagem de $A$, isto acontece quando, e somente quando, existe $i\in I$ tal que
%$x\in A_i$. Do mesmo modo, constatamos que $x$ é elemento da interseção de
%$(A_i)_{i\in I}$ se, e somente se, $x\in A_i$ para todo $i\in I$. Em símbolos
\[
\bigcup_{i\in I}A_i=\{x\ ;\ \text{existe }i\in I\text{ tal que }x\in A_i\}
\quad\text{e}\quad
\bigcap_{i\in I}A_i=\{x\ ;\ x\in A_i\text{ para todo }i\in I\}.
\]

\begin{exemplo} 
Sejam $A_i=(i,i+1)$ e
\(\displaystyle
B_i=(-{i^2}-1,{i^2}).
\)
Então:\\
\(\displaystyle
\bigcup_{i\in\Q}A_i=\R,
\)
\hfill
\(\displaystyle
\bigcap_{i\in\Q}B_i=(-1,0),
\)
\hfill
\(\displaystyle
\bigcap_{i\in\Q}A_i=\varnothing,
\)
\hfill
\(\displaystyle
\bigcup_{i\in\Q}B_i=\R,
\)
\hfill
\(\displaystyle
\bigcup_{i\in\Z}A_i=\R-\Z.
\)
%\(\displaystyle
%\bigcup_{q\in\Q}(q,q+1)=\R \qquad\text{ e } \qquad
%\bigcap_{q\in\Q}(-1/q^2,1/q^2)=\{0\}.
%\)
\end{exemplo} 

Se  $I$ é o conjunto dos números inteiros de $m$ até $n$, então também é usual
escrever
\[
\bigcup_{i=m}^nA_i=A_m\cup\dots\cup A_n\quad\text{e}\quad
\bigcap_{i=m}^nA_i=A_m\cap\dots\cap A_n.
\]
Se $I$ é o conjunto de todos os inteiros positivos, então as notações usuais são
\[
\bigcup_{i=1}^{+\infty}A_i
=\bigcup_{i\in\N}A_i
=A_1\cup A_2\cup\cdots\quad\text{e}\quad
\bigcap_{i=1}^{+\infty}A_i
=\bigcap_{i\in\N}A_i
=A_1\cap A_2\cap\cdots.
\]

O símbolo $\infty$%
\index{Infinito}
({\bf infinito})%
\index{Infinito}
que aparece nas notações anteriores não é um número. Ele é apenas um símbolo
tipográfico cujo papel é dizer que tanto a união quanto a interseção da família
$(A_i)_{i\in I}$ são tomadas para todo $i\in\{1,2,3,\dots\}$. Este mesmo símbolo
aparecerá em várias notações ao longo do texto sendo que em cada uma delas seu
papel será diferente. Porém, sempre devemos ter em mente que infinito não é
número!

