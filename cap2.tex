\chapter{Números naturais, inteiros e racionais}
\label{Cap: Numeros naturais, inteiros e racionais}

\section{Naturais, inteiros e indução.}
\label{Sec: Numeros naturais e inteiros}

O conjunto $\N=\{1,2,3,\dots\}$ é usado para contagens. De tão
natural, $\N$ é chamado de conjunto dos {\bf números naturais},
\index{Número!natural}%
\index{Conjunto!dos números!naturais}%
\index{N@$\N$}%
 o primeiro conjunto numérico que aparece na história de qualquer civilização
ou em qualquer tratado sobre os fundamentos da Matemática.

%Neste curso 
Admitiremos conhecidos os conjunto $\N$ e $\Z=\{\dots,-2,-1,0,1,2,
\dots\}$ (dos {\bf números inteiros})%
\index{Número!inteiro}%
\index{Conjunto!dos números!inteiros}
\index{Z@$\Z$}
bem como suas propriedades algébricas de soma e multiplicação e sua relação de
ordem $\le$.
Para um esboço da construção de $\N$ e $\Z$
leia as Seções \ref{Sec: construcaoN}
e \ref{Sec: construcaoZ} na p.\pageref{Sec: construcaoZ}.


No conjunto $\N$ valem dois princípios fundamentais: o ``Princípio da Boa
Ordem'' e o ``Princípio da Indução''. Vamos provar mais adiante que
são equivalentes.

\begin{principio} \nometeo{Da Indução (finita)}%
\index{Princípio!da Indução (finita)}
Seja $A\subset\N$ satisfazendo as seguintes propriedades:
\begin{align}
& 1\in A; \label{Eq: Um}\\
& n\in A\quad\text{implica que}\quad n+1\in A. \label{Eq: Sucessor}
\end{align}
Então $A=\N$.
\end{principio}
\begin{principio} \nometeo{Da Boa Ordem}%
\index{Princípio!da Boa Ordem}
Todo subconjunto não vazio de $\N$ possui {\bf elemento mínimo},%
\index{Elemento!mínimo}
ou seja, se $B\subset\N$ com $B\ne\varnothing$, então existe $n\in B$ tal que $n
\le m$ para todo $m\in B$.
\end{principio}

O Princípio da Indução (e suas variantes) é usado para demonstrar que certas
propriedades são verdadeiras para todo número natural. A estratégia é a
seguinte. Definimos o conjunto $A$ constituído pelos números naturais que
possuem uma certa propriedade $P$. A seguir, mostra-se que $A$ satisfaz
(\ref{Eq: Um}) e (\ref{Eq: Sucessor}). Daí, concluímos que $A=\N$ e, portanto,
que $P$ é verificada por todo número natural. Este tipo de argumento é chamado
de {\bf demonstração por indução}.
\indexinducao%
É conhecido por indução finita pois existe a indução transfinita
(veja \excpag{transfinita}).

\begin{exemplo} Vamos demonstrar, por indução, a conhecida fórmula $1+\dots+n=
n(n+1)/2$ válida para todo $n\in\N$. Seja $A$ o conjunto dos $n\in\N$ para os
quais a fórmula é valida, {\em i.e.},
\[
A=\left\{n\in\N\ \vline\ 1+\dots+n=\frac{n(n+1)}2\right\}.
\]
Pelo Princípio da Indução, basta mostrar que $A$ satisfaz (\ref{Eq: Um}) e
(\ref{Eq: Sucessor}) para concluir que $A=\N$, ou seja, que fórmula acima é
válida para todo $n\in\N$.

Evidentemente, $1\in A$ pois $1=1(1+1)/2$. Tomemos $n\in A$ e mostremos que $m=n
+1\in A$. Como $n\in A$ temos $1+\dots+n=n(n+1)/2$. Segue que
\[
1+\dots+m=1+\dots+n+(n+1)=\frac{n(n+1)}2+(n+1)=\frac{(n+1)(n+2)}2=\frac{m(m+1)}
2.
\]
\end{exemplo}

\begin{teorema}
\nometeo{Boa Ordem = Indução}
Vale o Princípio da Boa Ordem se, e somente se,
vale o Princípio da Indução.%
%Se vale o Princípio da Boa Ordem, então vale o Princípio da Indução.%
\index{Princípio!da Indução (finita)}%
\index{Princípio!da Boa Ordem}
\end{teorema}

\begin{dem}
Suponha válido o \underline{Princípio da Boa Ordem}.
Seja $A\subset\N$ satisfazendo (\ref{Eq: Um}) e (\ref{Eq: Sucessor}).
Suponhamos, \underline{por absurdo}, 
\indexabsurdo%
que $A\neq\N$. Isto significa que existe algum elemento
de $\N$ que não pertence a $A$ e, portanto, o conjunto $B=A^\complement$ é não
vazio. Pelo Princípio da Boa Ordem, $B$ possui um elemento mínimo
$m\in B$. Com
certeza $m>1$ pois como $1\in A$, $1\notin B=A^\complement$. 
Assim, $m-1$ é um natural menor que
$m$. Pela minimalidade de $m$, temos que $m-1\notin B$ e portanto $m-1\in A$. De
(\ref{Eq: Sucessor}) concluímos que $m=(m-1)+1\in A$, o que é absurdo.
%\end{dem}
%
%\begin{teorema}
%Se vale o Princípio da Indução, então vale o Princípio da Boa Ordem.
%\index{Princípio!da Indução}%
%\index{Princípio!da Boa Ordem}
%\end{teorema}
%
%\begin{dem}

Suponha válido o \underline{Princípio da Indução}.
Seja $B\subset\N$ não vazio. Suponhamos por absurdo que $B$ não possua elemento
mínimo. Em particular, $1\notin B$ (senão $1$ seria elemento mínimo de $B$).
Seja
\[
A=\{n\in\N\ ;\ n<m\ \forall m\in B\}.
\]

Observamos inicialmente que $A\cap B=\varnothing$. De fato, se $A\cap B\ne
\varnothing$, então existe $n\in A\cap B$. Tendo $n\in A$ temos também $n<m$
qualquer que seja $m\in B$, em particular, tomando $m=n\in B$ obtemos $n<n$ o
que é absurdo. Concluímos que $A\cap B=\varnothing$.

Mostraremos a seguir que $A=\N$. Vejamos agora que isto é suficiente para
concluir a demonstração. Neste caso temos $\varnothing=A\cap B=\N\cap B=B$
contradizendo a hipótese $B\neq\varnothing$.

Mostremos, por indução, que $A=\N$. Já sabemos que $1\notin B$ e portanto $1<m$
\indexinducao
qualquer que seja $m\in B$, ou seja, $1\in A$. Tomemos $n\in A$. Por definição
de $A$ temos $n<m$ qualquer que seja $m\in B$, logo $n+1\le m$ para todo $m\in
B$. Se $n+1\in B$ então $n+1$ é um elemento mínimo de $B$. Como, por hipótese,
$B$ não possui elemento mínimo, segue que $n+1\notin B$ e portanto $n+1<m$ para
qualquer $m\in B$. Concluímos  que $n+1\in A$. Pelo Princípio da Indução $A=
\N$.
\end{dem}

%\section{Conjuntos finitos, enumeráveis e não enumeráveis.}
\section{Cardinalidade.}

Como dissemos na Seção~\ref{Sec: Numeros naturais e inteiros} o conjunto $\N$ é
o conjunto usado para contagens. Quando queremos contar, por exemplo, o número
de integrantes do grupo {\em The Beatles} procedemos da seguinte maneira. A cada
músico associamos um elemento do conjunto $\N$ seguindo a sua ordem usual: Paul
1, John 2, George 3 e Ringo 4.

Acabamos de definir uma função injetiva $f$ do conjunto $A=\{ Beatles \}$ no
conjunto $\N$, de modo que $f(Paul)=1,\ f(John)=2,\ f(George)=3$ e $f(Ringo)=4$.
Bastava tomar o conjunto $B=\{1,2,3,4\}$ como contradomínio que $f$ ainda seria
injetiva. Porém, isto não seria possível se $B$ fosse $\{1,2,3\}$ pois, neste
caso, pelo menos um elemento de $B$ estaria associado a mais de um músico (e
portanto $f$ não seria injetiva). De fato, 4 é o menor número $n$ tal que o
conjunto $\{1,\dots,n\}$ possa ser contradomínio sem que $f$ deixe de ser
injetiva.
Estas considerações nos levam às seguintes definições:

\begin{definicao}
Dizemos que um conjunto $A$ é {\bf enumerável}%
\index{Conjunto!enumerável}
se ele é vazio ou se existe uma função injetiva $f:A\rightarrow\N$.
Caso contrário dizemos que $A$ é \textbf{não-enumerável}.
\end{definicao}

\begin{definicao}
Seja $A$ um conjunto não vazio. Se existe $n\in\N$ e uma função injetiva $g:A
\rightarrow\{1,\dots,n\}$ diremos que $A$ é {\bf finito},%
\index{Conjunto!finito}
caso contrário, $A$ é {\bf infinito}.%
\index{Conjunto!infinito}
O menor número $n$ que verifica esta propriedade é dito {\bf número de
elementos}%
\index{Número!de elementos}
de $A$. Escrevemos $\#A=n$. Diremos também que o conjunto vazio é finito e que
seu número de elementos é 0.
\end{definicao}

Observamos que o número de elementos de um conjunto finito $A$ não vazio é bem
definido graças ao Princípio da Boa Ordem. De fato, o conjunto dos números $n\in
\N$ que verificam a propriedade ``existe função injetiva $g:A\rightarrow\{1,
\dots,n\}$'' é um subconjunto não vazio (pois $A$ é finito) de $\N$ e portanto
possui um elemento mínimo.

Vejamos outro exemplo de contagem. Um professor vai aplicar uma prova e não tem
certeza se a sala destinada a este efeito tem um número suficiente de cadeiras
para acomodar os alunos. Ele pode contar as cadeiras e os alunos e comparar os
resultados para obter a resposta. Uma alternativa óbvia a este método é pedir
aos alunos que se acomodem e três coisas podem acontecer ao final do processo:

i. existem alunos de pé e todas as cadeiras estão ocupadas;

ii. existem cadeiras livres e todos os alunos estão sentados;

iii. todos os alunos estão sentados e todas as cadeiras estão ocupadas.

No primeiro caso temos que o número de alunos é maior que o de cadeiras, no
segundo caso ocorre o contrário e, finalmente, no terceiro eles são iguais.
Obtemos assim a resposta à pergunta ``qual conjunto tem mais elementos?'' sem
necessariamente conhecer os números de elementos dos conjuntos envolvidos. Estas
considerações motivam a seguinte definição.

\begin{definicao}
\label{Def: Cardinalidade}
Sejam $A$ e $B$ dois conjuntos não vazios. Dizemos que $A$ e $B$ têm a mesma
{\bf cardinalidade}%
\index{Cardinalidade}
ou que a cardinalidade de $A$ {\bf é igual} à de $B$ e escrevemos $\#A=\#B$, se
existe uma bijeção $f:A\rightarrow B$. Caso contrário dizemos que eles não têm a
mesma cardinalidade ou que suas cardinalidades são diferentes e escrevemos $\#A
\neq\#B$.
\end{definicao}

A definição anterior faz sentido mesmo se os conjuntos $A$ e $B$ são infinitos.
Nela o símbolo $\#A$ isoladamente não tem nenhum sentido. Apenas as expressões
$\#A=\#B$ e $\#A\neq\#B$ têm. Por outro lado, se $A$ é finito então $\#A$ é um
número natural e tendo eles a mesma cardinalidade temos que $\#A=\#B$ e esta
``igualdade'' tem dois sentidos distintos: como igualdade de números naturais e
como apresentado na Definição \ref{Def: Cardinalidade}. Porém a ``igualdade''
ocorre num sentido se, e somente se, ocorre no outro. Por esta razão, podemos
pensar no conceito de cardinalidade como generalização do conceito de número de
elementos.

\begin{definicao}
Sejam $A$ e $B$ conjuntos não vazios. Se existe função injetiva $f:A\rightarrow
B$, então dizemos que a {\bf cardinalidade}%
\index{Cardinalidade}
de $A$ é {\bf menor ou igual} à de $B$ e escrevemos $\#A\le\#B$. Se existe uma
função sobrejetiva $g:A\rightarrow B$, então dizemos que a {\bf cardinalidade}%
\index{Cardinalidade}
de $A$ é {\bf maior ou igual} a de $B$ e escrevemos $\#A\ge\#B$. Se $\#A\le\#B$
e $\#A\neq\#B$, então escrevemos $\#A<\#B$ (lê-se a cardinalidade%
\index{Cardinalidade}
de $A$ é {\bf menor} que a de $B$). Analogamente, se $\#A\ge\#B$ e $\#A\neq\#B$,
então escrevemos $\#A>\#B$ (lê-se a cardinalidade%
\index{Cardinalidade}
de $A$ é {\bf maior} que a de $B$).
\end{definicao}

Feita esta definição, temos que $A\neq\varnothing$ é enumerável se, e somente
se, $\#A\le\#\N$.

%É verdade que $\#A\le\#B$ se, e somente se, $\#B\ge\#A$ mas este fato carece
%de demonstração.
\begin{exemplo}
Seja $A$ um conjunto não vazio. É evidente que $\#A=\#A$ pois a função {\bf
identidade}%
\index{Função!identidade}
$Id:A\rightarrow A$ dada por $Id(x)=x$ para todo $x\in A$ é uma bijeção.
\end{exemplo}

\begin{exemplo}
Sejam $A$ e $B$ dois conjuntos não vazios com $A\subset B$. Obviamente $\#A\le
\#B$ pois a função $Id:A\rightarrow B$ dada por $Id(x)=x$ para todo $x\in A$ é
injetiva.
\end{exemplo}


\begin{proposicao}
\label{Prop: aleb}
Sejam $A$ e $B$ dois conjuntos não vazios. Então $\#A\le\#B$ se, e somente se,
$\#B\ge\#A$.
\end{proposicao}

\begin{dem}
Consequência do \excpag{finjetivasobre}: 
``Prove que 
existe $f:A\rightarrow B$ injetiva se, e somente se, 
existe $g:B\rightarrow A$ sobrejetiva.''
\end{dem}
%Suponhamos $\#A\le\#B$ e mostremos que $\#B\ge\#A$. Por definição, existe uma
%função injetiva $f:A\rightarrow B$. Para concluir, devemos mostrar que existe
%função sobrejetiva $g:B\rightarrow A$.
%
%Fixemos um elemento $y_0\in A$. Para todo $x\in B$ definimos $g(x)$ da seguinte
%maneira. Se $x\notin f(A)$ tomamos $g(x)=y_0$, senão, se $x\in f(A)$, então,
%pela injetividade de $f$, existe um único $y\in A$ tal que $f(y)=x$. Neste caso
%tomamos $g(x)=y$. Mostremos que $g$ é sobrejetiva. Seja $y\in A$ e $x=f(y)$.
%Temos $x\in f(A)$ e, por definição de $g$, segue que $g(x)=y$.
%
%Mostremos agora a recíproca, {\em i.e.}, que se $\#B\ge\#A$, então $\#A\le\#B$.
%Por hipótese, existe uma função sobrejetiva $g:B\rightarrow A$. Logo, para todo
%$y\in A$ podemos escolher $x\in B$ tal que $g(x)=y$. Definimos $f(y)=x$.
%Mostremos que $f$ é injetiva. Se $f(y_1)=f(y_2)$ (com $y_1,y_2\in A$), então
%$y_1=g(f(y_1))=g(f(y_2))=y_2$.

Outra propriedade que se espera do símbolo $\le$ é dada pelo teorema seguinte.
%Apresentamos sua demonstração e a comentaremos em seguida.

\footnotetext[1]{Georg Ferdinand Ludwig Philipp Cantor: $\star$ 03/03/1845, São Petersburgo, Rússia - $\dagger$ 06/01/1918 Halle, Alemanha.}%
\footnotetext[2]{Felix Bernstein: $\star$ 24/02/1878, Halle, Alemanha - $\dagger$ 03/12/1956, Zurique, Suíça.}%
\footnotetext[3]{Friedrich Wilhelm Karl Ernst Schröder: $\star$ 25/11/1841,  Mannheim, Alemanha - $\dagger$ 16/07/1902, Karlsruhe, Alemanha.}%

\begin{teoremastar} \nometeo{De 
Cantor\footnotemark
\index{Cantor}%
-Bernstein\footnotemark
\index{Bernstein}%
-Schröder\footnotemark
\index{Schröder}%
}%
\index{Teorema!de Cantor-Bernstein-Schröder}%
\label{Teo: Cantor-Bernstein-Schroeder}

Se $\#A\le\#B$ e $\#B\le\#A$, então $\#A=\#B$.
\end{teoremastar}
%A primeira vista esta demonstração pode parecer mirabolante. Vejamos que, de
%certa forma, ela é muito natural.

Antes de apresentar a demonstração, vamos comentar a ideia da prova.

O objetivo é construir uma bijeção $h$ de $A$ em $B$. Estão à nossa disposição
dois ingredientes: uma função $f$ de $A$ em $B$ e uma função $g$ de $B$ em $A$,
ambas injetivas. Existem, portanto, dois ``caminhos'' naturais que vão de $A$
até $B$: $f$ e $g^{-1}$. Considerando isto na definição de $h$, o problema
resume-se a decidir quais pontos de $A$ seguirão o primeiro caminho e quais
seguirão o segundo. Ou seja, dividimos $A$ em duas partes complementares, $X_0$
e $X_0^\complement$, e fazemos $h=f$ em $X_0$ e $h=g^{-1}$ em $X_0^\complement$.

A função $h$ será bijetiva se, e somente se, as imagens de $X_0$ e $X_0^
\complement$ forem complementares (em $B$). Ou seja, devemos escolher $X_0$ de
modo que $f\left(X_0\right)^\complement=g^{-1}\left(X_0^\complement\right)$ ou,
de modo equivalente, $g\left(f(X_0)^\complement\right)=X_0^\complement$. A
última equação é reescrita como $F(X_0)=X_0$, sendo $F$ definida por:
\(
F(X)=g\big(f(X)^\complement\big)^\complement.
\)

Por verificar $F(X_0)=X_0$, $X_0$ é dito \textbf{ponto fixo}%
\index{Ponto!fixo}
de $F$. Argumentos de ponto fixo são bastante usuais em Análise. A ideia,
intuitiva, é a seguinte. Considere uma função $F:Y\rightarrow Y$ para a qual
queremos encontrar um ponto fixo. Tomamos $y\in Y$ e iteramos $F$
``infinitas'' vezes obtendo o resultado $y_0$. Aplicando $F$ a $y_0$, teremos
como resultado $F$ iterada ``infinitas'' vezes, a partir de $y$, ou seja,
encontraremos novamente $y_0$. Portanto, $F(y_0)=y_0$. A descrição dada aqui
foge aos padrões de rigor da Matemática. A ideia de iterar ``infinitas'' vezes é
formalizada tomando a sequência
\(
F(y),\ F(F(y)),\ F(F(F(y))),\ \dots
\)
e verificando se ela tende a algum elemento que, naturalmente, esperamos ser
ponto fixo de $F$. 
%Para completar este programa, precisamos dos conceitos de
%limite e continuidade. Estes conceitos, fundamentais à Análise, serão explorados
%nos próximos capítulos.

\vspace{2ex}
\begin{dem}
Por hipótese, existem $f:A\rightarrow B$ e $g:B\rightarrow A$ injetivas.
Considere $F:{\cal P}(A)\rightarrow{\cal P}(A)$ dada por
\[
F(X)=g\big(f(X)^\complement\big)^\complement\quad\forall X\subset A.
\]
Seja $\displaystyle X_0=\bigcap_{i=0}^{+\infty}F^i(A)$ 
(convencionando que $F^0(A)=A$). Como
$f$ é injetiva, temos
\[
f(X_0)=f\left(\bigcap_{i=0}^{+\infty}F^i(A)\right)=\bigcap_{i=0}^{+\infty}
f\big(F^i(A)\big).
\]
Portanto,
\begin{align*}
F(X_0)&=g\left(\left(\bigcap_{i=0}^{+\infty}f\big(F^i(A)\big)\right)^\complement
\right)^\complement
=g\left(\bigcup_{i=0}^{+\infty}f\big(F^i(A)\big)^\complement\right)^\complement
=\left(\bigcup_{i=0}^{+\infty}g\left(f\big(F^i(A)\big)^\complement\right)\right)
^\complement\\
&=\bigcap_{i=0}^{+\infty}g\left(f\big(F^i(A)\big)^\complement\right)^\complement
=\bigcap_{i=0}^{+\infty}F\big(F^i(A)\big)
=\bigcap_{i=1}^{+\infty}F^i(A)
=\bigcap_{i=0}^{+\infty}F^i(A)=X_0.
\end{align*}
Segue que $X_0^\complement=F(X_0)^\complement=g\big(f(X_0)^\complement\big)$.
Concluímos que $g$ é uma bijeção de $f(X_0)^\complement$ em $X_0^ \complement$,
logo, $g^{-1}$ é uma bijeção de $X_0^\complement$ em $f(X_0)^\complement$.
Também temos que $f$ é uma bijeção de $X_0$ em $f(X_0)$. Destas observações
segue que $h:A\rightarrow B$ dada por
\[
h(x)=\left\{
\begin{array}{ll}
f(x) & \text{se }x\in X_0,\\
g^{-1}(x) & \text{se }x\in X_0^\complement,
\end{array}
\right.
\]
é bijetiva.
\end{dem}


Na demonstração anterior não foi necessário considerar limites pois é
natural dizer que uma sequência de conjuntos encaixantes:
\(
A_1\supset A_2\supset A_3\supset\dots
\)
``converge'' para $\displaystyle\bigcap_{n=1}^{+\infty}A_n$.
Veja o 
\excpag{liminfsupconj} 
para definição de limite de
sequências de conjuntos.

\begin{observacao} 
Outra propriedade que se espera do símbolo $<, =$ e $>$ entre
cardinalidades  é que,
dados $A$ e $B$ dois 
	conjuntos quaisquer 
	vale 
	(um resultado difícil)
	a \textbf{tricotomia da cardinalidade}:
	\index{Tricotomia da Cardinalidade}
	$\# A=\# B$ ou $\# A>\# B$ ou
	$\# A<\# B$.
Veja \excpag{tricotomia}.
\end{observacao} 

\begin{exemplo}
\label{Ex: Z e enumeravel}
$\#\Z=\#\N$. Escrevendo $\Z=\{0,1,-1,2,-2,3,-3,\dots\}$ uma bijeção de $f:\N
\rightarrow\Z$ nos salta aos olhos\index{Olhos}. Ela é dada por $f(1)=0, f(2)=1,\ f(3)=-1,\
f(4)=2,\ f(5)=-2,\ f(6)=3,\ \dots$, mais precisamente,
\[
f(n)=\left\{
\begin{array}{rll}
m & \text{se }n=2m, & m=1,2,3,\dots\\
-m & \text{se }n=2m+1, & m=0,1,2,\dots
\end{array}
\right.
\]
\end{exemplo}

\begin{proposicao}
\label{proposicao: N^2 e enumeravel}
$\N^2$ é enumerável.  
\end{proposicao}
\begin{dem} 
Pela unicidade da fatoração de
naturais como produto de primos, (Teorema Fundamental da Aritmética)%
\index{Teorema!Fundamental!da Aritmética}
temos que a função $g:\N^2\rightarrow\N$ dada por $g(m,n)=2^m3^n$ é injetiva.
\end{dem}

\begin{exemplo}
%Mais precisamente 
$\#\N^2=\#\N$. 
%$\#\N^2=\#\N$, em particular, $\N^2$ é enumerável. 
De fato, $\#\N\le\#\N^2$
pois a função $f:\N\rightarrow\N^2$ dada por $f(n)=(n,n)$ é claramente injetiva.
Por outro lado, 
pela Proposição~\ref{proposicao: N^2 e enumeravel},
$\#\N^2\le\#\N$. Pelo Teorema~\ref{Teo: Cantor-Bernstein-Schroeder}
(Cantor-Bernstein-Schöreder), $\#\N^2=\#\N$.

Outra demonstração  que $\#\N^2=\#\N$,
bastante popular e de caráter geométrico,
é obtida através do
esquema mostrado na Figura \ref{Fig: Pauling}.
\begin{figure}[ht]
  \fbox{
    \parbox{.97\textwidth}{
      \bf
      \bigskip
      \hfill
      \begin{tabular}{ccccccc}
        \rnode{p1}{(1,1)}  & \rnode{p2}{(1,2)} & \rnode{p4}{(1,3)} &
        \rnode{p7}{(1,4)}  & \rnode{p11}{(1,5)} & $\cdots$ \\ \\

        \rnode{p3}{(2,1)}  & \rnode{p5}{(2,2)} & \rnode{p8}{(2,3)} &
        \rnode{p12}{(2,4)} & $\ddots$ \\ \\

        \rnode{p6}{(3,1)}  & \rnode{p9}{(3,2)} & \rnode{p13}{(3,3)} &
        $\ddots$ \\ \\

        \rnode{p10}{(4,1)}  & \rnode{p14}{(4,2)} & $\ddots$ \\ \\

        \rnode{p15}{(5,1)}  & $\ddots$ \\ \\

        $\vdots$
      \end{tabular}
      \hfill\
      \ncline{->}{p1}{p2}
      \ncline{->}{p2}{p3}
      \ncline{->}{p3}{p4}
      \ncline{->}{p4}{p5}
      \ncline{->}{p5}{p6}
      \ncline{->}{p6}{p7}
      \ncline{->}{p7}{p8}
      \ncline{->}{p8}{p9}
      \ncline{->}{p9}{p10}
      \ncline{->}{p10}{p11}
      \ncline{->}{p11}{p12}
      \ncline{->}{p12}{p13}
      \ncline{->}{p13}{p14}
      \ncline{->}{p14}{p15}
      \caption{Bijeção de $\N$ em $\N^2$.}
      \label{Fig: Pauling}
      \bigskip
    }
  }
\end{figure}
Uma bijeção $h:\N\rightarrow\N^2$ é definida seguindo as setas da seguinte
maneira:
\(
h(1)=(1,1),\quad h(2)=(1,2),\quad h(3)=(2,1),\quad h(4)=(1,3),\quad h(5)=(2,2),
\quad\dots
\)
\end{exemplo}

\begin{proposicao}
\nometeo{argumento diagonal de Cantor}
\index{Argumento diagonal de Cantor}%
\index{Cantor!argumento diagonal}%
\label{proposicao: card N<card P(N)}%
$\P(\N)$  é não-enumerável. 
%Mais precisamente, 
%$\#\N<\#\P(\N)$. 
%$\#\N<\#\P(\N)$. 
\end{proposicao}
\begin{dem}
%A função $f:\N\rightarrow
%\P(\N)$ dada por $f(n)=\{n\}$ é obviamente injetiva, logo, 
%$\#\N\le\#\P(\N)$. 
%Vamos completar a demonstração com duas provas distintas, uma direta e
%outra por contradição.
Pela Proposição~\ref{Prop: aleb} basta mostrar que,
dada uma função $g:\N \rightarrow\P(\N)$ qualquer,
$g$ não pode ser sobrejetiva.

IDEIA: Considere a lista de conjuntos $g(1), g(2), g(3), \ldots$
Construa conjunto $A$  tal que:

$\bullet$ $1\in A$ sse $1\not\in g(1)$;

$\bullet$ $2\in A$ sse $2\not\in g(2)$;

$\bullet$ $3\in A$ sse $3\not\in g(3)$;

$\bullet$
\vdots\qquad\vdots\qquad\vdots\qquad\vdots\quad.
%\qquad\vdots\qquad\vdots\qquad\vdots

Assim,  por construção, 
$A\neq g(1)$, 
$A\neq g(2)$, 
$A\neq g(3)$, \ldots
Portanto $A\neq g(n)$ para todo $n$ e $g$ não é sobrejetiva.
Isto é conhecido como argumento diagonal de Cantor.

\textbf{Com rigor:}
defina $A=\big\{n\in\N\ ;\ n\notin g(n)\big\}\in\P(\N)$. 
Como $n\in A$ se, e somente se, $n\not\in g(n)$, concluímos que
$g(n)\neq A$ para todo $n\in\N$.
Logo $g$ não é sobrejetiva.
\end{dem}


\begin{observacao} 
Fazer o  \excpag{exc: argumento diagonal}
do argumento diagonal de Cantor generalizado.
\end{observacao} 

%(b) Suponhamos, \underline{por absurdo},
%\index{Demonstração por absurdo}%
%\index{Absurdo, demonstração por}
%que exista uma bijeção $g:\N
%\rightarrow\P(\N)$. Seja $A=\big\{n\in\N\ ;\ n\notin g(n)\big\}$. Como
%$g$ é bijetiva, existe $m\in\N$ tal que $g(m)=A$. Das duas, uma: ou $m\in A$ ou
%$m\notin A$. Se $m\in A$, então $m\notin g(m)=A$, que é absurdo! Por outro lado,
%se $m\notin A$, então $m\in g(m)=A$ o que também é absurdo. Concluímos que não

%\begin{observacao} 
%Note que a tricotomia da cardinalidade (\excpag{tricotomia})
%e $\P(\N)$  não-enumerável implicam que
%%Mais precisamente, 
%$\#\N<\#\P(\N)$. 
%Como não provamos a tricotomia, começamos provando que
%$\#\N\le\#\P(\N)$.
%\end{observacao} 

O argumento diagonal de Cantor 
usado na Proposição \ref{proposicao: card N<card P(N)} 
lembra muito o Paradoxo de Russel. 
%Ele é conhecido pelo nome de ``Processo de Diagonalização de Cantor''.
Georg Cantor foi o primeiro matemático a se interessar pelas questões de
cardinalidade. A ele devemos este conceito. Ele procurou, sem sucesso, um
conjunto $A$ tal que $\#\N<\#A<\#\P(\N)$. Finalmente ele conjeturou que
não existia tal conjunto: a chamada ``Hipótese do Contínuo''.%
\index{Hipótese do contínuo}%
\label{hipcontinuo}
Demonstrá-la ou encontrar contraexemplo foi o primeiro da lista de 16
problemas não resolvidos no século XIX que, segundo Hilbert%
\footnote{David Hilbert: $\star$ 23/01/1862, Kaliningrad, Rússia - $\dagger$
14/02/1943, Göttingen, Alemanha.}%
\index{Hilbert},
seriam os principais a serem estudados no século XX. A questão foi totalmente
resolvida em 1963. Numa primeira etapa, em 1940, Gödel%
\footnote{Kurt Gödel: $\star$ 28/04/1906, Brno, República Tcheca - $\dagger$
14/01/1978, Princeton, Estados Unidos.}%
\index{Gödel}
\cite{Godel} mostrou que ele era consistente com os axiomas de Teoria dos
Conjuntos propostos por Zermelo%
\footnote{Ernst Friedrich Ferdinand Zermelo: $\star$ 27/07/1871, Berlim,
Alemanha - $\dagger$ 21/05/1953, Freiburg, Alemanha.}%
\index{Zermelo}
e Fraenkel%
\footnote{Adolf Abraham Halevi Fraenkel: $\star$ 17/02/1891, Munique, Alemanha -
$\dagger$ 15/10/1965, Jerusalém, Israel.},%
\index{Fraenkel}
ou seja, Gödel mostrou que não era possível demonstrar que a Hipótese do
Contínuo era falsa. Finalmente, em 1963, Cohen%
\footnote{Paul Joseph Cohen: $\star$ 02/04/1934, Long Branch, Estados Unidos.}%
\index{Cohen}
\cite{Cohen} mostrou que, por outro lado, não era possível mostrar que ela era
verdadeira! Desta forma demonstrou-se que a Hipótese do Contínuo é independente
dos axiomas da Teoria dos Conjuntos.
Um exemplo do uso desta hipótese é o \excpag{Exc: hipcontinuo}.

\begin{proposicao}
\nometeo{união de enumeráveis é enumerável}
\label{Prp: Uniao finita enumeravel}
Se $A$ e $B$ são enumeráveis, então $A\cup B$ é enumerável.
\end{proposicao}

\begin{dem}
Se $A=\varnothing$ ou $B=\varnothing$, então a proposição é imediata. Suponhamos
que ambos sejam não vazios. Então, existem funções injetivas $f:A\rightarrow\N$
e $g:B\rightarrow\N$. Definimos $h:A\cup B\rightarrow\N$ da seguinte maneira:
\[
h(x)=\left\{
\begin{array}{ll}
2f(x) & \text{se }x\in A,\\
2g(x)+1 & \text{se }x\in B\setminus A.
\end{array}
\right.
\]
Temos que $h$ é bem definida e é, claramente, injetiva (observe que $h(A)\cap
h(B)=\varnothing$ pois os elementos de $h(A)$ são números pares enquanto que os
de $h(B\setminus A)$ são ímpares).
\end{dem}

Esta Proposição é generalizada pela próxima Proposição.

\begin{proposicao}
\nometeo{união enumerável de enumeráveis é enumerável}
\label{Prp: Uniao enumeravel enumeravel}
Se, para cada $n\in\N$, $A_n$ é enumerável, então $\bigcup_{n=1}^{+\infty}A_n$ é
enumerável.
\end{proposicao}

\begin{dem}
Sem perda de generalidade, podemos supor que $A_n\neq\varnothing$ para todo $n
\in\N$. Seja $A=\bigcup_{n=1}^{+\infty}A_n$.
Por hipótese, para cada $n\in\N$, temos que $A_n$ é enumerável, logo, existe
$f_n:\N\rightarrow A_n$ sobrejetiva. Vamos mostrar que a função
\[
\begin{array}{rccl}
f: & \N\times\N  & \longrightarrow & A\\
   & (n,m) & \longmapsto     & f_n(m)
\end{array}
\]
é sobrejetiva. De fato, se $x\in A$, então existe $n\in\N$ tal que $x\in A_n$.
Como $f_n$ é sobrejetiva, existe $m\in\N$ tal que $f_n(m)=x$. Segue que $f(n,m)=
f_n(m)=x$.
Na Proposição \ref{proposicao: N^2 e enumeravel} 
vimos que $\#\N=\#\N^2$. Portanto, existe
$g:\N\rightarrow\N^2$ sobrejetiva. Segue que $f\circ g:\N\rightarrow A$ é
sobrejetiva.
\end{dem}

\section{$\star$ O Hotel de Hilbert}
\label{Sec: O Hotel de Hilbert}

David Hilbert foi grande entusiasta das descobertas de Cantor, chegando a
afirmar que ``ninguém nos expulsará do paraíso que Cantor criou para nós''. Para
ilustrar o conceito de infinitude e enumerabilidade, Hilbert imaginou um hotel
de infinitos quartos. Vamos explorar a ideia de Hilbert com uma dose (extra) de
ficção.

O Hotel de Hilbert fica ao bordo do Mar Mediterrâneo, em Saint Tropez, na
badalada Cote d'Azur. Seu edifício, cinza e branco, construído em 1925 é um belo
exemplo do estilo {\it art-déco} dos anos 20 e 30 do século XX. Grande e
confortável, o hotel tem uma infinidade enumerável de quartos suficientes para
hospedar clientes dos mais diversos gostos. Desde aqueles em busca de dias
tranquilos e ensolarados aos que preferem noites em boîtes agitadas. O gerente,
o próprio David Hilbert, é um homem muito gentil, de barba bem tratada que nunca
é visto sem seus óculos e chapéu branco.

Como é alta temporada, o hotel está lotado. Porém, o painel localizado em sua
entrada informa que há vagas disponíveis! Chega um homem de camiseta florida,
carregando uma pequena e elegante valise marrom. Ele pede um quarto a Hilbert
que responde:

-- Apesar do hotel estar completamente lotado, providenciarei um quarto vazio
para o senhor. Aguarde um minuto, por favor.

Aproveitando que os hóspedes são muito solícitos, pelo alto-falante,
\index{Alto-falante}%
Hilbert se
dirige a eles:

-- Perdoem-me por incomodá-los. Gostaria de pedir a cada um de vocês que troque
de quarto. Quem está ocupando o quarto $n$ passará ao quarto $n+1$. Grato pela
compreensão.

E o cliente, satisfeito, se instala no quarto número 1.

A época é de muita procura. Chega um ônibus de excursão com uma infinidade
enumerável de cadeiras. Todas estão ocupadas mas, de acordo com as estritas
normas de segurança do lugar, ninguém viaja em pé. O animador do grupo,
facilmente reconhecível por sustentar uma pequena flâmula vermelha com a marca
da agência, dirige-se a Hilbert solicitando os quartos que havia reservados para
seus clientes. 

Confirmando a reserva, Hilbert solicita um minuto para providenciar os quartos.
Novamente pelo alto-falante, dirige-se aos hóspedes:

-- Perdoem-me por incomodá-los outra vez. Peço novamente que troquem de quarto,
desta vez, obedecendo a seguinte regra: quem estiver ocupando o quarto $n$
mudará para o quarto $2n$. Mais uma vez, agradeço a compreensão.

Hilbert informa ao animador que ele seu grupo podem acomodar-se. Quem está na
cadeira $m$ ocupará o quarto $2m-1$.

Fim do verão e o hotel se esvazia. Outra excursão chega. O animador, com
bandeira amarela, é menos experiente que seu colega e não reservou os quartos
antecipadamente pois acreditava em baixa ocupação no outono. O ônibus está
cheio mas, novamente, não há pessoas em pé. Além disto, para cada número real há
uma cadeira no ônibus com aquele número! Surpreendentemente, Hilbert informa
que, apesar do hotel estar completamente vazio, não há vagas suficientes para
acomodar a todos. E, amavelmente, sugere o \textbf{Hotel Real}
que é maior que o seu.

No próximo capítulo veremos porque Hilbert não podia receber o último grupo.

\section{Racionais: operações, enumerabilidade e ordem.}
%\section{Números racionais: operações e enumerabilidade.}
\label{Sec: Numeros racionais: operacoes}

Lembramos que um {\bf número racional}%
\index{Número!racional}
é aquele que pode ser expresso como razão entre dois inteiros $m,n\in\Z$, com
$n\ne0$, {\em i.e.},
\[
\forall x\in\Q,\quad\exists m\in\Z,\ n\in\N\quad\text{tais que}\quad x=\frac mn.
\]
$\Q$%
\index{Q@$\Q$}
é o conjunto dos {\bf números racionais}.%
\index{Conjunto!dos números!racionais}
Como $m/1=m$ para todo $m\in\Z$ temos que $\Z\subset\Q$.

Como fizemos com $\N$ e $\Z$ admitiremos neste curso que o leitor já está
familiarizado com as propriedades básicas do conjunto $\Q$. 
Para um esboço da construção de $\Q$ 
leia a \secpag{Sec: construcaoQ}.
Nesta e nas próximas
duas seções revisaremos algumas destas propriedades e estudaremos outras menos
familiares.

\begin{proposicao}
\label{Prp: Q e enumeravel}
$\Q$ é enumerável %, {\em i.e.}, 
e $\#\N=\#\Q$.
\end{proposicao}
\begin{dem}
Como $N\subset\Z\subset\Q$, temos que $\#\N\le\#\Q$. Vamos mostrar que $\#\N\ge
\#\Q$.
A definição de número racional diz que a função $f:\Z\times\N\rightarrow\Q$ dada
por $f(m,n)=m/n$ é sobrejetiva.
Vimos no Exemplo \ref{Ex: Z e enumeravel} que $\Z$ é enumerável. Segue do
\excpag{Exc: Produto finito de enumeraveis e enumeravel}
que $\Z\times\N$
também é enumerável. Logo existe $g:\N\rightarrow\Z\times\N$ sobrejetiva.
Terminamos a demonstração observando que $f\circ g:\N\rightarrow\Q$ é
sobrejetiva.
Para outra prova ver \excpag{Exc: Qenumeravel}.
\end{dem}

As operações de adição e multiplicação de números racionais verificam certas
propriedades algébricas que definem o conceito de corpo.

\begin{definicao}
\label{Def: Corpo}
Seja $\K$ um conjunto munido de duas operações binárias chamadas {\bf adição}%
\index{Adição!em um corpo}
e {\bf multiplicação}%
\index{Multiplicação!em um corpo}
da seguinte maneira: a cada par $x,y\in\K$ a adição e a multiplicação fazem
corresponder, respectivamente, a sua {\bf soma}%
\index{Soma!em um corpo}
$x+y\in\K$ e o seu {\bf produto}%
\index{Produto!em um corpo}
$x\cdot y\in\K$ (por simplicidade, às vezes omitimos o ``$\cdot$''). 
Dizemos que o
terno $(\K,+,\cdot)$ é um {\bf corpo} se valem as seguintes propriedades.

i.
\(
x+y=y+x\quad\text{e}\quad x\cdot y=y\cdot x\quad\forall x,y\in\K
\)
(comutatividade).%
\index{Comutatividade}
%da adição e da multiplicação:

ii.
\(
(x+y)+z=x+(y+z)\quad\text{e}\quad(x\cdot y)\cdot z=x\cdot(y\cdot z)\quad\forall
x,y,z\in\K
\)
(associatividade).%
\index{Associatividade}
%da adição e da multiplicação:

iii.
\(
\exists!x\in\K\)
tal que
\(x+y=y\quad\forall y\in\K\)
% \label{It: Existencia do zero}
(existência do elemento neutro da adição).%
\index{Elemento!neutro!da adição}
O elemento neutro $x$ será denotado $0$ e chamado de {\bf zero}.%
\index{Zero}

iv.
\(
\forall x\in\K,\quad\exists!y\in\K\quad\text{tal que}\quad x+y=0
\)
(existência de oposto).%
\index{Oposto}
O elemento $y$ que é o \textbf{oposto} de $x$ será denotado por $-x$.

v.
\(
\exists!x\in\K\setminus\{0\}\quad\text{tal que}\quad x\cdot y=y\quad\forall y\in
\K
\)
 (existência do elemento neutro da multiplicação).
O elemento neutro $x$ será denotado $1$ e chamado de {\bf um}.
\index{Elemento!neutro!da multiplicação}

vi.
\(
\forall x\in\K\setminus\{0\},\quad\exists!y\in\K\quad\text{tal que}\quad x\cdot
y=1
\)
(existência de inverso).%
\index{Inverso}
O elemento $y$ que é o \textbf{inverso} de $x$ será denotado por $x^{-1}$.

vii.
\(
x\cdot(y+z)=(x\cdot y)+(x\cdot z)\quad\forall x,y,z\in\K
\)
(distributividade).%
\index{Distributividade}
\end{definicao}

A multiplicação tem prioridade sobre a soma: $x\cdot y+x
\cdot z$ significa $(x\cdot y)+(x\cdot z)$.

\begin{exemplo}
O terno $(\Q,+,\cdot)$, onde $+$ e $\cdot$ são as operações usuais de adição e
multiplicação (de números racionais), é um corpo.%
\index{Corpo}
\index{Corpo!dos números!racionais}
\end{exemplo}

A Propriedade %(\ref{It: Existencia do zero}) 
(iii)
nos diz que zero existe e é único.
Na verdade a unicidade do zero pode ser demonstrada a partir de sua existência,
{\em i.e.}, poderíamos substituir o símbolo ``$\exists!$'' por ``$\exists$'' que
não faria diferença. De fato, suponhamos que $0$ e $0'$ sejam dois zeros, ou
melhor, dois elementos neutros da adição. Mostraremos que $0=0'$. Como $0$ é
elemento neutro da adição, $0+y=y$ para todo $y\in\K$. Em particular, para $y=
0'$, temos $0+0'=0'$. Da mesma maneira, obtemos que $0'+0=0$. Portanto, $0'=0+0'
=0'+0=0$.

Analogamente a existência do oposto de $x$ implica a sua unicidade. De fato,
suponhamos que $y$ e $z$ são opostos de $x$. Isto significa que $x+y=0$ e $x+z=
0$, logo $x+y=x+z$. Adicionando $y$ aos dois lados da equação obtemos
\[
y+x+y=y+x+z\quad\imp\quad(y+x)+y=(y+x)+z\quad\imp\quad0+y=0+z\quad\imp\quad y=z.
\]

Cabe ao leitor a tarefa de verificar as unicidades de $1$ e do inverso.

Da definição de oposto e da comutatividade da soma, temos que $x$ é o oposto de
$y$ se, e somente se, $y$ é o oposto de $x$. Em outros termos, o oposto de $-x$
é $x$, ou ainda $-(-x)=x$. Observação análoga vale para o inverso.

Para simplificar a escrita, usaremos as seguintes convenções:
\[
x-y=x+(-y)\quad\text{e}\quad\frac xy=x/y=x\cdot y^{-1}.
\]

\begin{observacao} 
Além dos corpos famosos como $\Q, \R, \Co$, existem outros:
extensões de $\Q$
(\excpag{Exc: extensaoQ}), 
corpo dos números algébricos
(\excpag{Exc: corpo algebricos}),
corpos finitos
(\excpag{Exc: corpofinito}),
quatérnios
(\excpag{Exc: quaternios}). 
\end{observacao} 

As operações de um corpo podem ser estendidas às funções com contradomínio
neste corpo. Este é o objeto da próxima definição.

\begin{definicao}
Sejam $(\K,+,\cdot)$ um corpo e $f,g:A\rightarrow\K$. As funções {\bf soma},%
\index{Função!soma}
{\bf produto},%
\index{Função!produto}
{\bf diferença}%
\index{Função!diferença}
e {\bf quociente}%
\index{Função!quociente}
de $f$ e $g$ são definidas e denotadas, respectivamente, por

i. $(f+g)(x)=f(x)+g(x)$ para todo $x\in A$;

ii. $(f\cdot g)(x)=f(x)\cdot g(x)$ para todo $x\in A$;

iii. $(f-g)(x)=f(x)-g(x)$ para todo $x\in A$;

iv. $(f/g)(x)=f(x)/g(x)$ para todo $x\in A$ tal que $g(x)\neq 0$.\\
Dado $c\in\K$ definimos ainda $(c\cdot f)(x)=c\cdot f(x)$ para todo $x\in A$.
\end{definicao}

%\section{Números racionais: ordem.}
\label{Sec: Numeros racionais: ordem}

No conjunto dos números racionais está definida uma relação de ordem completa.

\begin{definicao}
\label{Def: Ordem}
Uma relação $\le$ num corpo $(\K,+,\cdot)$ é dita {\bf ordem total} ou,
simplesmente, {\bf ordem}%
\index{Ordem}
se valem as seguintes propriedades.

i. se $x\le y$ e $y\le z$, então $x\le z$ (transitiva).

ii. se $x\le y$ e $y\le x$, então $x=y$ (antissimétrica).

iii. $\forall x,y\in\K$ temos $x\le y$ ou $y\le x$ (completa).

iv. se $x\le y$, então $x+z\le y+z\quad\forall z\in\K$ (adição é
monótona).

v. se $x\le y$, então $x\cdot z\le y\cdot z$
quando $0\le z$ e $y\cdot z\le x\cdot z$ quando $z\le 0$
(multiplicação é monótona).

Neste caso, dizemos que $(\K,+,\cdot,\le)$ é um {\bf corpo ordenado}.%
\index{Corpo!ordenado}
\end{definicao}

\begin{definicao}
Seja $(\K,+,\cdot,\le)$ um corpo ordenado e sejam $x,y\in\K$. Se $x\le y$, então
dizemos que $x$ é {\bf menor ou igual} a $y$, ou ainda, que $y$ é {\bf maior ou
igual} a $x$ e também escrevemos $y\ge x$. Se $x\le y$ e $x\neq y$, então
dizemos que $x$ é {\bf menor} que $y$ e escrevemos $x<y$, ou ainda, que $y$ é
{\bf maior} que $x$ e escrevemos $y>x$.
\end{definicao}

\begin{definicao}
\label{Def: Conjunto limitado}
Sejam $(\K,+,\cdot,\le)$ um corpo ordenado e $A\subset\K$. Dizemos que $A$ é
{\bf limitado superiormente}%
\index{Conjunto!limitado!superiormente}
pela {\bf cota superior}%
\index{Cota!superior}
$s\in\K$ se $a\le s$ para todo $a\in A$. Caso contrário, $A$ é {\bf ilimitado
superiormente}.%
\index{Conjunto!ilimitado!superiormente}
De modo análogo define-se conjunto {\bf limitado inferiormente},%
\index{Conjunto!limitado!inferiormente}
{\bf cota inferior}%
\index{Cota!inferior}
e conjunto {\bf ilimitado inferiormente}.%
\index{Conjunto!ilimitado!inferiormente}
Finalmente, $A$ é dito {\bf limitado}%
\index{Conjunto!limitado}
se ele é limitado superior e inferiormente. Caso contrário, $A$ é {\bf
ilimitado}.%
\index{Conjunto!ilimitado}

\end{definicao}

\begin{definicao}
\label{Def: Funcao limitada}
Sejam $(\K,+,\cdot,\le)$ um corpo ordenado e $f:A\rightarrow\K$. Dizemos que
$f$ é {\bf limitada superiormente}%
\index{Função!limitada!superiormente}
se $f(A)$ é limitado superiormente. Analogamente define-se função {\bf limitada
inferiormente}%
\index{Função!limitada!inferiormente},
{\bf função limitada}%
\index{Função!limitada}
e {\bf função ilimitada}.%
\index{Função!ilimitada}
\end{definicao}

\begin{definicao}
Sejam $(\K,+,\cdot,\le)$ um corpo ordenado, $A\subset\K$ e $f:A\rightarrow\K$.

i. $f$ é {\bf crescente}%
\index{Função!crescente}
quando $x<y$ implica que  $f(x)\le f(y)$.

ii. $f$ é {\bf decrescente}%
\index{Função!decrescente}
quando $x<y$ implica que $f(y)\le f(x)$.

iii. $f$ é {\bf monótona}%
\index{Função!monótona}
quando é crescente ou decrescente.

iv. $f$ é {\bf estritamente crescente}%
\index{Função!estritamente!crescente}
quando $x<y$ implica que $f(x)<f(y)$.

v. $f$ é {\bf estritamente decrescente}%
\index{Função!estritamente!decrescente}
quando $x<y$ implica que $f(x)>f(y)$.

vi. $f$ é {\bf estritamente monótona}%
\index{Função!estritamente!monótona}
quando é estritamente crescente ou estritamente decrescente.
\end{definicao}

\section{$\star$ Corpos Arquimedianos.}
%\section{$\star$ Números racionais: propriedade arquimediana.}
\label{Sec: Numeros racionais: propriedade arquimediana}

Uma importante propriedade do corpo ordenado $(\Q,+,\cdot,\le)$ é ser
arquimediano.
Para isto, dado um corpo qualquer $\K$ com $1$ elemento neutro da
multiplicação,  definimos por
$\widetilde\N$ o conjunto $\{1,\ 1+1, \ 1+1+1, \ldots \}$.
É claro que para $\K=\Q$ ou $\R$, $\widetilde\N=\N$.

\begin{definicao}
Dizemos que um corpo ordenado $(\K,+,\cdot,\le)$ é {\bf arquimediano}%
\index{Corpo!arquimediano}
se $\widetilde\N$ é um subconjunto de $\K$ ilimitado superiormente, ou seja, para todo
$x\in\K$ existe $m\in\widetilde\N$ tal que $x<m$.
\end{definicao}

De fato, $(\Q,+,\cdot,\le)$ é arquimediano pois se $x\in\Q$, com $x>0$, então,
existem $m\in\Z$ e $n\in\N$ tais que $x=m/n$. Como $x>0$, temos $m\in\N$.
Concluímos observando que $x=m/n\le m<m+1\in\N$.
Um exemplo de corpo não-arquimediano é o corpo $\Z_p$ com $p$ primo
(veja \excpag{Exc: corpofinito}).
