# Curso de Análise Real (livro)

ISBN: 978-65-86502-04-6

Pode-se gerar o livro no formato PDF (a4) com make.

## AUTORES

Cassio Neri e Marco A. P. Cabral (professores do Instituto de Matemática da UFRJ)

## DESCRIÇÃO

Um livro de análise real para curso de graduação contendo muitos 
exercícios em cada capítulo e um guia de estudo.

## SÍTIOS

Publicado em versão digital em novembro de 2021 pela Editora do Instituto de Matemática da UFRJ. Acesse
aqui o PDF do livro: 
[Link da Editora do IM](https://www.im.ufrj.br/index.php/pt/editora-im/matematica)

Visite [Link](https://sites.google.com/matematica.ufrj.br/mapcabral/livros-e-videos)
para ter acesso a outros materiais relacionados a este livro.

## HISTÓRIA DAS VERSÕES

    * Primeira Edição digital de novembro de 2021 pela Editora do Instituto de Matemática da UFRJ.
    * Versão 2.4 Dezembro 2011 -- Retirados diversos erros ao longo do texto; Fonte LaTeX convertida de isolatin para UTF8.
    * Versão 2.3 Julho 2010 -- Acrescentado mais 2 exercicios no cap.2
    * Versão 2.2 Dezembro 2009 -- Adaptação ao acordo ortográfico
    * Versão 2.1 Dezembro 2008 -- retirado erros exercicios; 2 novas proposições
    * Versão 2.0 Julho 2008 -- Entrada de Marco Cabral; introduzido novo capítulo com a construção dos conjuntos numéricos (N, Z, Q, R, C); adição de 260 exercícios.
    * Versão 1.0 2006 -- Versão inicial do livro por Cassio Neri.

## LICENÇA

Creative Commons CC BY-NC-SA 3.0 BR, Atribuição (BY) Uso Não-Comercial (NC) CompartilhaIgual (SA) 3.0 Brasil.
Veja arquivo LICENSE para detalhes.
