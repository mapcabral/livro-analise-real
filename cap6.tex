\chapter{Topologia de $\R$}

\section{Introdução.}

A seguinte frase é facilmente aceita pela nossa intuição: ``se $x$ é um número
próximo de 2, então $x^2$ é um número próximo de $4$''. Outra, ``$x^2$ estará
cada vez mais próximo de $4$ quanto mais próximo $x$ estiver de 2''. Por esta
razão dizemos que a função $f(x)=x^2$ (para todo $x\in\R$) é {\bf contínua}%
\index{Função!contínua!em um ponto}
no ponto $2$. Muitas das funções que encontramos na Análise são funções
contínuas. Queremos precisar o conceito de continuidade. Observe que para isto é
necessário estabelecer o que queremos dizer com ``$x$ é um número próximo de
2''.

Inicialmente, observe que a noção de ``estar próximo'' usada cotidianamente é
uma noção subjetiva. Por exemplo, um aluno, 
morador de Niterói-RJ,
que está no Pão-de-açúcar, 
perguntado onde é a Praia de Ipanema, possivelmente responderá:
``fica  longe, você tem que pegar um ônibus''. 
Por outro lado, se o mesmo aluno viaja para Ribeirão Preto e lá o
perguntarem em qual cidade ele mora, então, temendo que os ribeirenses não
conheçam Niterói, ele resolve precisar sua resposta dizendo:
``fica perto da cidade do Rio de Janeiro''. 
Finalmente, o mesmo aluno numa viagem espacial, perguntado onde mora,
responderá: ``no planeta Terra, perto da Estrela Sol''. 
Na primeira frase, o longe significa uns 4 km, na segunda frase
o perto significa uns 15 km e, na terceira, o perto significa uns
$10^5$ km.
\index{Sol}\index{Terra}\index{Niterói}\index{Perto}\index{Longe}
\index{Ipanema}

%do Rio de Janeiro
%de Engenharia de
%Produção da UFRJ, morador de Niterói, responda a um colega ao ser perguntado
%onde é o COPPEAD. Possivelmente ele responderá ``É longe. Fica depois da
%reitoria''. Por outro lado, se o mesmo aluno viaja para Ribeirão Preto e lá o
%perguntarem em qual cidade ele mora, então, temendo que os ribeirenses não
%conheçam Niterói, ele resolve precisar sua resposta dizendo que ``fica perto da
%cidade do Rio de Janeiro''. Certamente o aluno sabe que a distância entre o
%bloco F do CT e o COPPEAD é menor que os 14 km da ponte Presidente Costa e Silva
%(a popular Rio-Niterói) que separam as duas cidades.

Em Matemática, como em qualquer outra ciência, as ideias intuitivas e subjetivas
são muito bem vindas para ajudar a tornar conceitos abstratos em objetos mais
``palpáveis''. Tais ideias facilitam a compreensão e o desenvolvimento do
conhecimento. Entretanto, em definições e demonstrações, devemos lidar apenas
com conceitos e fatos rigorosos e objetivos. Ideias que dependam de
interpretação do leitor, de acordo com sua opinião, não fazem parte de nenhuma
teoria matemática. É claro que, mesmo em Matemática, opiniões e divergências de
opiniões existem. Porém, uma demonstração (ou contraexemplo) acaba com qualquer
polêmica sobre a veracidade de uma afirmação.

Para evitar esta subjetividade no conceito de proximidade, podemos refrasear o
exemplo dizendo que ``a medida que $x$ se aproxima de 2, $x^2$ se aproxima de
4'', ou ``se $x$ tende a 2, então $x^2$ tende a 4''. O verbo {\em tender} nos
faz pensar imediatamente no conceito de limite que já foi explorado no capítulo
anterior. Resumindo: os conceitos de proximidade e limite estão intimamente
relacionados.

A {\bf Topologia}%
\index{Topologia}
é o ramo da Matemática que trata destas questões de limite (e/ou proximidade). A
Topologia da Reta, isto é, a Topologia de $\R$, é bem simples, para não dizer
pobre. Nela, os abstratos conceitos da Topologia Geral ganham formas mais
concretas e compreensíveis. Poderíamos usar estas formas simplificadas em nossa
exposição porém, preferimos argumentos mais gerais para facilitar a (futura)
passagem do leitor ao estudo da Topologia em contextos mais gerais. Mesmo que o
leitor não venha a se especializar em Topologia, para se aprofundar em Análise
ou Geometria serão necessários outros conhecimentos que ultrapassam os da
Topologia da Reta.

\section{Conjuntos abertos e conexos.}
%\section{Pontos interiores e conjuntos abertos.}

Intuitivamente, $x$ é um ponto no interior de um conjunto $A$ se os pontos {\em
vizinhos} a $x$ (tanto à esquerda quanto à direita) também estão em $A$. Mais
precisamente temos:

\begin{definicao}
\label{Def: Ponto interior}
Dizemos que $x\in\R$ é {\bf ponto interior}%
\index{Ponto!interior}
de $A\subset\R$ (ou que $A$ é {\bf vizinhança}%
\index{Vizinhança}
de $x$) se $A$ contém um intervalo aberto do qual $x$ é elemento. Neste caso,
escrevemos $x\in A^\circ$, ou seja, $A^\circ$ é o conjunto dos pontos interiores
de $A$,  denominado {\bf interior}%
\index{Interior}%
\index{Conjunto!dos pontos!interiores}
de $A$.
\end{definicao}

\begin{observacao}
\label{Obs: Vizinhanca}
É fácil ver que na definição anterior podemos substituir, sem perda de
generalidade, o intervalo aberto arbitrário por um intervalo da forma $(x-\ep,
x+\ep)$ com $\ep>0$. Ou, em outros termos, $x\in A^\circ$ se, e somente se, 
\[
\exists\ep>0\quad\text{tal que}\quad|y-x|<\ep\quad\imp\quad y\in A.
\]
\end{observacao}

Temos sempre $A^\circ\subset A$. Porém a inclusão inversa não é necessariamente
verdadeira. Tomemos, por exemplo, $A=[0,1]$. Temos que $1\notin A^\circ$ pois
todo intervalo aberto que contém 1 tem elementos maiores que 1 e portanto não
está contido em $A$.

É trivial que todo ponto de um intervalo aberto pertence ao interior do
intervalo. Ou seja, se $A$ é um intervalo aberto e não vazio, então $A^\circ=A$.
De maneira geral temos a seguinte definição.

\begin{definicao}
\label{Def: Aberto}
Um conjunto $A$ é {\bf aberto}%
\index{Conjunto!aberto}%
\index{Aberto}
se todos os seus pontos são interiores, ou seja, se $A\subset A^\circ$ (neste
caso, $A^\circ=A$).
\end{definicao}

Como na Observação~\refpag{Obs: Vizinhanca} temos que $A$ é aberto se, e somente
se, 
\[
\forall x\in A,\quad\exists\ep>0\quad\text{tal que}\quad|y-x|<\ep\quad\imp\quad
y\in A.
\]

Vamos introduzir aqui a notação de conjuntos para denotar intervalos. 
Ela é importante pois 
é mais compacta e
generaliza os conceitos topológicos para o $\R^n$. 

\begin{definicao} 
\label{Def: bola}\index{Bola}%
Dado $x_0\in\R$ e $\ep>0$ qualquer, denotamos por $B_\ep(x_0)$ o
conjunto (um intervalo aberto)\index{Intervalo} 
$\{x\in\R; \; |x-x_0|<\ep \}$. Assim $B_\ep(x_0)=(x_0-\ep, x_0+\ep)$.
\end{definicao} 
 
Desta forma,  $B_\ep(x_0)$  é 
 um intervalo centrado em $x_0$ de raio $\ep$.  
Em $\R^n$, $B_\ep(x_0)$ é uma bola centrada em $x_0$ de raio $\ep$.
Com esta notação podemos redefinir conjunto aberto de tal forma
que a mesma definição seja válida em $\R^n$.
%\[
%\forall x\in A,\quad\exists\ep>0\quad\text{tal que}\quad y\in B_\ep(x)\quad\imp\quad
%y\in A,
%\]
%ou ainda,

\begin{proposicao}
\label{Def: Aberto2}
Um conjunto $A$ é {\bf aberto}%
\index{Conjunto!aberto}%
\index{Aberto}
se, e somente se, 
\[
\forall x\in A,\quad\exists\ep>0\quad\text{tal que}\quad
B_\ep(x)\subset A.
\]
\end{proposicao}
\begin{dem} 
Deixo para o leitor.
\end{dem} 


\begin{exemplo}
O conjunto vazio é aberto! De fato, negar esta afirmação significa admitir que
$\varnothing^\circ\subsetneq\varnothing$ e, em particular, admitir que existe
$x\in\varnothing$.
\end{exemplo}

\begin{exemplo}
O conjunto $[0,1]$ não é aberto pois, como já vimos, $1\notin[0,1]^\circ$. Da
mesma maneira, $0\notin[0,1]^\circ$. Por outro lado, qualquer $x\in(0,1)$ é
interior de $[0,1]$ ou seja $[0,1]^\circ=(0,1)$.
\end{exemplo}

As propriedades mais importantes dos conjuntos abertos são dadas no teorema
abaixo.

\begin{teorema}
\nometeo{propriedades de abertos}
\label{Teo: Abertos em geral}
Temos:

%\item\label{It: Abertos triviais}
i.
os conjuntos $\varnothing$ e $\R$ são abertos;

%\item\label{It: Reuniao de abertos}
ii.
toda união de abertos é aberta;

%\item\label{It: Intersecao de abertos}
iii.
toda interseção finita de abertos é aberta.
\end{teorema}

\begin{dem}
%(\ref{It: Abertos triviais})
(i) Já foi provado.

%(\ref{It: Reuniao de abertos}) 
(ii) Sejam $(A_i)_{i\in I}$ uma família de abertos e
$A=\bigcup_{i\in I}A_i$. Se $x\in A$, então existe $i\in I$ tal que $x\in A_i$.
Como $A_i$ é aberto, 
existe $\ep>0$ tal que
$B_\ep(x)\subset A_i\subset A$. Segue que $A$ é aberto.
%temos $x\in A_i^\circ$, logo existe $\ep>0$ tal que
%$(x-\ep,x+\ep)\subset A_i\subset A$. Segue que $x\in A^\circ$.

%(\ref{It: Intersecao de abertos}) 
(iii) 
Basta mostrar que se $A_1$ e $A_2$ são dois
conjuntos abertos então $A=A_1\cap A_2$ também é aberto (o caso geral segue por
indução).
\indexinducao%
Se $A=\varnothing$, então não há nada mais a ser demonstrado.
Suponhamos $A\ne\varnothing$ e seja $x\in A$. Temos que $x\in A_1$ e $x\in A_2$,
logo, existem $\ep_1,\ep_2>0$ tais que 
$B_{\ep_i}(x)\subset A_i$ ($i=1,2$).
Tomando $\ep=\min\{\ep_1,\ep_2\}$ obtemos que 
$B_\ep(x)\subset A$, ou seja,
$A$ é aberto.
%$(x-\ep_i,x+\ep_i)\subset A_i$ ($i=1,2$).
%Tomando $\ep=\min\{\ep_1,\ep_2\}$ obtemos que $(x-\ep,x+\ep)\subset A$, ou seja,
%$x\in A^\circ$.
\end{dem}


\begin{definicao} 
\label{Def: conexo}
%Em Topologia, 
Dizemos que  
$A\subset\R$ 
é um  \textbf{conjunto conexo} 
se $A$ é um dos
intervalos da Definição~\refpag{Def: Intervalo}. 
\index{Conjunto!conexo}%
\end{definicao} 

Em Topologia mais geral (em $\R^n$ por exemplo), definimos
\textbf{conjunto conexo}
utilizando apenas conjuntos abertos. Para detalhes ver 
\excpag{Exc: conexo}.


\section{Conjuntos fechados e discretos.}
%\section{Pontos de aderência e conjuntos fechados.}

\begin{definicao}
\label{Def: Ponto de aderencia}
Dizemos que $x\in\R$ é {\bf ponto de aderência}%
\index{Ponto!de aderência}
de $F\subset\R$ se existe uma sequência $\seqx\subset F$ tal que $x_n\rightarrow
x$. Neste caso, escrevemos $x\in\overline{F}$, ou seja, $\overline{F}$ é o
conjunto dos pontos de aderência de $F$ e também é chamado de {\bf fecho}%
\index{Fecho}%
\index{Conjunto!dos pontos!de aderência}
de $F$.
\end{definicao}

É fácil ver que $x$ é ponto de aderência de $F$ se, e somente se, 
dado qualquer
$\ep>0$, $B_\ep(x)$ tem pontos de $F$.
%intervalo aberto da forma $(x-\ep,x+\ep)$, onde $\ep>0$, tem pontos de $F$.

Temos sempre $F\subset\overline{F}$. Porém a inclusão inversa não é
necessariamente verdadeira. Tomemos, por exemplo, $F=[0,1)$. Temos $1\in
\overline{F}$ pois a sequência $x_n=1-1/n$ é convergente para 1 e além disto
$x_n\in F$ para todo $n\in\N$.

Seja $\seqx$ uma sequência convergente para $x$. Sabemos que se $x_n\ge a$ para
todo $n\in\N$, então $x\ge a$. Do mesmo modo, se $x_n\le b$ para todo $n\in
\N$, então $x\le b$. Conclui-se que uma sequência convergente de pontos em um
intervalo fechado tem o seu limite no intervalo. Ou seja, se $F$ é um intervalo
fechado e não vazio, então $\overline{F}=F$.

\begin{definicao}
\label{Def: Fechado}
Um conjunto $F$ é {\bf fechado}%
\index{Conjunto!fechado}
se todos os seus pontos de aderência pertencem a $F$, ou seja, se $\overline{F}
\subset F$ (que neste caso implica $F=\overline{F}$).
\end{definicao}

\begin{exemplo}
O conjunto vazio é fechado por vacuidade\index{Vacuidade}! 
De fato, negar esta afirmação significa admitir que
existe ponto de aderência que não pertence a $\varnothing$. Mas o vazio não
possui pontos para violar esta condição. Logo a afirmação é satisfeita por
vacuidade.
%$\varnothing\subsetneq\overline{\varnothing}$ e, em particular, admitir que
%existe $\seqx\subset\varnothing$.
\end{exemplo}

\begin{exemplo}
O conjunto $(0,1)$ não é fechado pois, como já vimos, $1\in\overline{(0,1)}$. Da
mesma maneira $0\in\overline{(0,1)}$. Por outro lado, se $\seqx\subset(0,1)$
é convergente para $x$ então $x\in[0,1]$. Segue que $\overline{(0,1)}=[0,1]$.
\end{exemplo}

O conjunto vazio (e também $\R$) são exemplos de conjuntos que são abertos e
fechados simultaneamente. Isto nos mostra, que ao contrário do que podem sugerir
as palavras ``aberto'' e ``fechado'', estes dois conceitos não são excludentes.
Além disso um conjunto pode não ser aberto nem fechado.
Refraseando Observação~\refpag{Obs: portas}, conjuntos
\textbf{não são portas} \sorriso. 
\index{Portas}
Porém, existe uma relação estreita entre conjuntos abertos e conjuntos fechados.

\begin{proposicao}
\nometeo{aberto é complementar de fechado}
\label{Prop: abertofechado}
Um conjunto é aberto se, e somente se, seu complementar é fechado.
\end{proposicao}

\begin{dem}
Seja $A\subset\R$ e $F=A^\complement$.

Suponhamos que $A$ seja aberto e mostremos que $F$ é fechado. Para isto, devemos
mostrar que $\overline{F}\subset F$. Se, \underline{por absurdo},
\indexabsurdo%
existir uma sequência
$\seqx\subset F$ convergente para $x\notin F$ ({\em i.e.}, $x\in A$), então,
como $A$ é aberto, existe $\ep>0$ tal que 
$B_\ep(x)\subset A$. Desta
%$(x-\ep,x+\ep)\subset A$. Desta
maneira, para $n$ suficientemente grande, temos que 
$x_n\in B_\ep(x)\subset A$. 
%$x_n\in (x-\ep,x+\ep)\subset A$. 
Isto é absurdo pois $x_n\in F=A^\complement$ para todo $n\in\N$.

Suponhamos agora que $F$ seja fechado e mostremos que $A$ é aberto. Se $A$ não
for aberto, então existirá $x\in A$ tal que $x\notin A^\circ$. Assim, qualquer
que seja $\ep>0$, 
 $B_\ep(x)$ não estará contido em $A$. Em
%o intervalo $(x-\ep,x+\ep)$ não estará contido em $A$. Em
particular, para cada $n\in\N$, tomando $\ep=1/n$ concluímos que existe 
$x_n\in B_{1/n}(x)$ 
%$x_n\in (x-1/n,x+1/n)$ 
tal que $x_n\notin A$, ou seja, $x_n\in F$. Vemos facilmente que
$x_n\rightarrow x$ e, portanto, $x\in\overline{F}$. Como $F$ é fechado, temos $x
\in F$, o que é absurdo pois $x\in A=F^\complement$.
\end{dem}

\begin{observacao}
\label{Obs: Fechados em geral}
Tomando complementares, o Teorema \ref{Teo: Abertos em geral} nos diz que

%\item os conjuntos $\varnothing$ e $\R$ são fechados;
%      \label{It: Fechados triviais}
%\item toda união finita de fechados é fechada;
%      \label{It: Reuniao de fechados}
%\item toda interseção de fechados é fechada.
%      \label{It: Intersecao de fechados}

i. os conjuntos $\varnothing$ e $\R$ são fechados;

ii. toda união finita de fechados é fechada;

iii. toda interseção de fechados é fechada.
\end{observacao}

Um conceito relacionado ao de ponto de aderência e de muita importância é dado
na definição seguinte.

\begin{definicao}
\label{Def: Ponto de acumulacao}
Dizemos que $x\in\R$ é {\bf ponto de acumulação}%
\index{Ponto!de acumulação}
de $F\subset\R$ se existe uma sequência $\seqx\subset F\setminus\{x\}$ tal que
$x_n\rightarrow x$, ou, em outros termos, se $x\in\overline{F\setminus\{x\}}$.
\end{definicao}

A ideia desta definição é que se $x$ é ponto de acumulação de $F$ então $x$
pode ser ``aproximado'' por elementos de $F$, diferentes de $x$.

Segue imediatamente da definição que todo ponto de acumulação é também ponto de
aderência. Porém, a recíproca não é verdadeira. Por isto, consideramos também a
seguinte definição.

\begin{definicao}
\label{Def: Ponto isolado}
Se $x$ é ponto de aderência de $F$ e não é ponto de acumulação, 
então $x$ é dito {\bf ponto isolado}%
\index{Ponto!isolado}
de $F$.
\end{definicao}

\begin{definicao}
\label{Def: conjunto discreto}%
\index{Conjunto!discreto}%
Um conjunto é \textbf{discreto} se todos os seus
	pontos são isolados. 
\end{definicao}

Tente entender o porquê desta nomenclatura.

\section{Conjuntos compactos.}

A próxima definição é apenas uma entre várias maneiras de se definir conjuntos
compactos em $\R$. 
Estas várias definições, dependendo do contexto ({\em i.e.},
do espaço topológico),
podem não ser equivalentes (neste caso, a definição dada
neste texto é a da chamada compacidade sequencial). Porém, como já dissemos
anteriormente, a topologia da reta é bastante simples e neste contexto tais
definições são equivalentes.

Dependendo dos objetivos de cada um, pode-se usar uma ou outra forma de
compacidade. A escolha pela definição seguinte é, de certa maneira, uma escolha
pessoal do autor baseada em sua própria experiência em Matemática. É provável
que outro autor, mais interessado em Geometria do que em Equações a Derivadas
Parciais, prefira outra definição.

\begin{definicao}
Um conjunto não-vazio $K\subset\R$ é {\bf compacto}%
\index{Conjunto!compacto}%
\index{Compacto}
se toda sequência de pontos de $K$ tem uma subsequência convergente para um
ponto de $K$.
\end{definicao}

Vejamos uma caracterização bem simples e de uso prático para conjuntos
compactos.

\footnotetext[1]{Heinrich Eduard Heine: $\star$ 16/03/1821, Berlim, Alemanha
- $\dagger$ 21/10/1881, Halle, Alemanha.}%
\footnotetext[2]{Félix Edouard Justin Emile Borel: $\star$ 07/01/1871, Saint Affrique,
França - $\dagger$ 03/02/1956, Paris, França.}%
\begin{teorema} 
\nometeo{Heine\footnotemark
\index{Heine}%
-Borel\footnotemark
\index{Borel}%
}%
\index{Teorema!de Heine-Borel}%
\label{Teo: Heine-Borel}
Um subconjunto não-vazio 
de $\R$ é compacto se, e somente se, ele é fechado e limitado.
\end{teorema}

\begin{dem}
Pelo Teorema de Bolzano-Weierstrass, toda sequência num conjunto limitado tem
subsequência convergente. Se além de limitado o conjunto é fechado, então o
limite desta subsequência será um elemento do conjunto. Isto mostra que todo
fechado e limitado é compacto.

Suponhamos agora que $K\subset\R$ seja compacto e mostremos que ele é limitado e
fechado. Sejam $x\in\overline K$ e $\seqx\subset K$ convergente para $x$. Como
qualquer subsequência de $\seqx$ tende a $x$ (Proposição \ref{Prp: x_n converge
-> x_n_k converge}), graças à compacidade, temos $x\in K$. Segue que $K$ é
fechado. Suponhamos, \underline{por absurdo}, 
\indexabsurdo%
que $K$ não seja limitado, digamos,
superiormente. Então, para cada $n\in\N$ existe $x_n\in\K$ tal que $x_n>n$.
Temos que $\seqx\subset K$ e $x_n\rightarrow+\infty$. Portanto, todas as suas
subsequências tendem a $+\infty$ (veja a 
Observação~\refpag{Obs: x_n converge -> x_n_k converge}) e, portanto, não são convergentes. Isto contradiz a compacidade
de $K$.
\end{dem}

A última demonstração (sobretudo a primeira parte) é digna de um livro de
Topologia Geral. Em vários destes livros as demonstrações usam muito texto e
poucos símbolos (algarismos, em particular). Na opinião do autor, além da
importância incontestável da Topologia Geral, estes livros também são
referências perfeitas para mostrar aos leigos em Matemática que, ao contrário do
que eles pensam, nós não somos pessoas que trabalham fazendo contas com
algarismos (números, como eles dizem)!
\sorriso 

Terminamos esta seção com outra caracterização de compactos. Mesmo não sendo
útil neste curso, tal caracterização é importantíssima. Em Topologia Geral, esta
caracterização é a definição de compacidade. Antes, definiremos cobertura
aberta.

\begin{definicao}
Uma {\bf cobertura aberta}%
\index{Cobertura aberta}
para $K$ é uma coleção $\C$ de conjuntos abertos tais que
\[
K\subset\bigcup_{A\in\C}A
\]
\end{definicao}

\begin{teorema}
\nometeo{caracterização de compactos por cobertura}
\label{Teo: Compacto por cobertura}
Um conjunto $K$ é compacto se, e somente se, toda cobertura aberta $\C$ para $K$
tem subcobertura finita, ou seja, existe $\C'\subset\C$ finita que é cobertura
para $K$.
\end{teorema}

Antes de demonstrar este teorema, em toda sua generalidade, mostraremos um
caso particular.

\footnotetext[1]{Henri Léon Lebesgue: $\star$ 28/05/1875, Beauvais, France - $\dagger$
26/07/1941, Paris, França.}%
\begin{teorema} 
\nometeo{Borel-Lebesgue\footnotemark
\index{Lebesgue}%
}
\label{Teo: Borel-Lebesgue}
Se $\C$ é um cobertura aberta para $[a,b]$, então ela tem subcobertura finita.
\end{teorema}

\begin{dem}
Note a semelhança desta prova com a apresentada no 
\excpag{Exc: Bolzano}.

Procedemos \underline{por absurdo}, 
\indexabsurdo%
supondo que $\C$ não tenha subcobertura finita.

Dividindo o intervalo $[a,b]$ no seu ponto médio obtemos dois intervalos de
comprimento $(b-a)/2$. Para pelo menos um destes intervalos, que denotaremos
$[a_1,b_1]$, não existe subcobertura de $\C$ finita. De fato, se existissem
$\C',\C''\subset\C$ finitas que fossem coberturas para o primeiro e para o
segundo intervalo, respectivamente, então $\C'\cup\C''$ seria uma subcobertura
finita de $\C$ para $[a,b]$. Aplicamos o procedimento anterior ao intervalo
$[a_1,b_1]$. Continuando indefinidamente este processo construímos uma sequência
$\big([a_n,b_n]\big)_{n\in\N}$ de intervalos encaixantes. Além disto, qualquer
que seja $n\in\N$, $b_n-a_n=(a-b)/2^n$ e não existe subcobertura finita de $\C$
para $[a_n,b_n]$.

Graças ao Teorema dos Intervalos Encaixantes, temos que $\bigcap_{n=1}^{+\infty}
[a_n,b_n]\ne\varnothing$. Mais precisamente, esta interseção só tem um elemento
$x$. De fato, suponhamos que exista $y\ne x$ tal que $y\in[a_n,b_n]$ para todo
$n\in\N$. Segue $0<|x-y|\le b_n-a_n$ para todo $n\in\N$. Isto é absurdo já que
$b_n-a_n\rightarrow 0$.

Ora, $x\in[a,b]$, logo, existe $A\in\C$ tal que $x\in A$. Como $A$ é aberto,
existe $\ep>0$ tal que 
$B_\ep(x)\subset A$. 
%$(x-\ep,x+\ep)\subset A$. 
Tomando $N\in\N$,
suficientemente grande, de modo que $b_N-a_N<\ep$ temos 
$[a_N,b_N]\subset B_\ep(x)\subset A$. 
%$[a_N,b_N]\subset(x-\ep, x+\ep)\subset A$. 
Portanto, tomando $\C'=\{A\}$, temos que $\C'$ é uma
subcobertura finita de $\C$ para $[a_N,b_N]$. Absurdo!
\end{dem}

\

\begin{dem} 
\nometeo{do Teorema \ref{Teo: Compacto por cobertura}}
Suponhamos que $K$ seja compacto (portanto limitado e fechado). Seja $\C$ uma
cobertura aberta de $K$. Como $K$ é limitado podemos tomar $a,b\in\R$ tais que
$K\subset[a,b]$. Como $K$ é fechado, o conjunto $K^\complement$ é aberto. Temos
claramente que $\C\cup\{K^\complement\}$ é uma cobertura aberta de $[a,b]$.
Pelo Teorema de Borel-Lebesgue, existe 
$\C'\subset\C$ finita tal que 
$\displaystyle K\subset[a, b]\subset\bigcup_{A\in\C'}A\cup\{K^\complement\}$. Daí, 
concluímos que $\displaystyle K\subset \bigcup_{A\in\C'}A$.

Suponhamos agora que toda cobertura aberta de $K$ possua subcobertura finita.
Para todo $x\in K$ definimos 
$A_x=B_1(x)$. 
%$A_x=(x-1,x+1)$. 
A coleção $\{A_x\ ;\ x\in K\}$ é
uma cobertura aberta de $K$. Por hipótese, existem $x_1<\dots<x_n\in K$ tais que
$K\subset A_{x_1}\cup\dots\cup A_{x_n}$. Logo, 
$K\subset(x_1-1,x_n+1)$ e,
portanto, $K$ é limitado.

Vamos mostrar que $K^\complement$ é aberto para concluir que $K$ é fechado e,
portanto, compacto (pois já sabemos que ele é limitado). Seja $y\in K^
\complement$. Para todo $x\in K$ definimos
\[
A_x=\left(x-\frac{|x-y|}2,x+\frac{|x-y|}2\right).
\]
Temos que $(A_x)_{x\in K}$ é uma cobertura aberta de $K$ tal que $y\notin A_x$
qualquer que seja $x\in K$. Por hipótese, existem $x_1,\dots,x_n\in K$ tais que
$K\subset A_{x_1}\cup\dots\cup A_{x_n}$. Tomando
\[
\ep=\frac12\min\{|x_1-y|,\dots,|x_n-y|\},
\]
é fácil ver que 
$B_\ep(y)\subset K^\complement$. 
%$(y-\ep,y+\ep)\subset K^\complement$. 
Mostramos que $y\in(K^
\complement)^\circ$ e, portanto, $K^\complement$ é aberto.
\end{dem}

\section{Conjuntos densos.}

\begin{definicao}
\label{Def: Conjunto denso}
Sejam $A,B\subset\R$ com $A\subset B$. Dizemos que $A$ é {\bf denso}%
\index{Conjunto!denso}%
\index{Denso}
em $B$ se $B\subset\overline A$.
\end{definicao}

Em outros termos, se $A\subset B$, então $A$ é denso em $B$ se, e somente se,
para todo $x\in B$, existe $\seqx\subset A$ tal que $x_n\rightarrow x$.
A próxima proposição nos fornece uma condição necessária e suficiente para a
densidade.

\begin{proposicao}
\nometeo{densos e abertos}
\label{Propo: denso}
Sejam $A,B\subset\R$. Temos que $A$ é denso em $B$ se, e somente se, todo
%intervalo 
aberto que contém algum ponto de $B$ também contém algum ponto de $A$.
\end{proposicao}

\begin{dem}
Suponhamos que $A$ seja denso em $B$. Sejam $x\in B$ e $\seqx\subset A$
convergente para $x$. Se $I$ um %intervalo 
aberto contendo $x$, então para $n\in
\N$ suficientemente grande temos $x_n\in I$. Portanto $I\cap A\ne\varnothing$.

Por outro lado, suponhamos que todo aberto que intercepta $B$ também intercepte
$A$. Seja $x\in B$. Para todo $n\in\N$, o intervalo aberto $(x-1/n,x+1/n)$
contém $x\in B$ e, portanto, contém algum ponto $x_n\in A$. Definimos desta
maneira uma sequência $\seqx\subset A$ tal que $x_n\rightarrow x$. Segue que $x
\in\overline A$. Logo, $B\subset\overline A$.
\end{dem}


Vejamos um dos exemplos mais importantes de conjuntos densos em $\R$.

\begin{exemplo}
\label{Ex: Q e denso}
$\Q$ é denso em $\R$. De fato, sejam $a,b\in\R$ com $a<b$. Mostremos que $(a,b)
\cap\Q\ne\varnothing$. Se $0\in(a,b)$, então não há mais nada a ser demonstrado.
Se $0\notin(a,b)$, então $0\le a$ ou $b\le0$. Consideremos o caso $a\ge0$ (o
caso $b\le0$ é análogo). Como $\R$ é arquimediano, existe $n\in\N$ tal que $n>1
/(b-a)$. Seja $m\in\N$ o menor natural tal que $m>na$, ou seja, $m\in\N$
satisfaz
\[
\frac{m-1}n<a<\frac mn.
\]
Para concluir que $m/n\in(a,b)\cap\Q$ basta mostrar que $m/n<b$. Suponhamos, 
\underline{por absurdo}, 
\indexabsurdo%
que $m/n>b$. Neste caso,
\[
\frac{m-1}n<a<b<\frac mn\quad\imp\quad b-a<\frac mn-\frac{m-1}n\quad\imp\quad
b-a<\frac1n.
\]
Contradizendo $n>1/(b-a)$.
\end{exemplo}


Todos os conceitos básicos de topologia podem ser
definidos utilizando somente o conceito 
de conjunto aberto. De fato, um conjunto é:

(a) \textbf{fechado}, pelo 
Teorema~\ref{Prop: abertofechado}, se seu complementar é aberto;


(b)  \textbf{compacto}, pelo 
Teorema~\ref{Teo: Compacto por cobertura},
se toda cobertura aberta possui subcobertura finita;

(c) \textbf{denso} em $B$, pela
Proposição~\ref{Propo: denso}, 
se todo aberto que intercepta $B$ também intercepta
o conjunto;

(b) \textbf{conexo}, pelo 
\excpag{Exc: conexo}, se não é união disjunta
de abertos não-vazios.

Este é um ponto de vista mais
avançado: introduzir conjuntos abertos e definir  fechado, compacto,
denso e conexo
diretamente, sem usar, por exemplo, sequências.

